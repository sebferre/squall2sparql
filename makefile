OCAMLFIND=ocamlfind
CMO=cmo
CMA=cma
OCAMLC=$(OCAMLFIND) ocamlc
# for developing purposes
#LIB_DIR=../lib
LIB_DIR=lib
INCLUDES= -I $(LIB_DIR) -I $(LIB_DIR)/dcg -I $(LIB_DIR)/ipp  # all relevant -I options here
OCAMLFLAGS= -pp "camlp5o -I $(LIB_DIR)/dcg -I $(LIB_DIR)/ipp pa_dcg.cmo pa_ipp.cmo" -w -u-s-y -thread $(INCLUDES)    # add other options for ocamlc here

SQUALL_SRC = semantics.ml syntax.ml sparql.ml backtranslation.ml dbpedia.ml
SQUALL_OBJ = $(SQUALL_SRC:.ml=.$(CMO))

COMMON_OBJ = str.$(CMA) nums.$(CMA) unix.$(CMA) bintree.$(CMO) common.$(CMO) lSet.$(CMO)

all: $(LIB_DIR)/common.$(CMO) $(LIB_DIR)/dcg/dcg.$(CMA) $(LIB_DIR)/ipp/ipp.$(CMA) script backscript interpreter

$(LIB_DIR)/common.$(CMO):
	cd lib && make

$(LIB_DIR)/dcg/dcg.$(CMA):
	cd lib/dcg && make

$(LIB_DIR)/ipp/ipp.$(CMA):
	cd lib/ipp && make

script: $(SQUALL_OBJ) squall2sparql.$(CMO)
	$(OCAMLC) $(INCLUDES) -g -custom -o squall2sparql $(COMMON_OBJ) dcg.$(CMA) ipp.$(CMA) $(SQUALL_OBJ) squall2sparql.$(CMO)

backscript: $(SQUALL_OBJ) sparql2squall.$(CMO)
	$(OCAMLC) $(INCLUDES) -g -custom -o sparql2squall $(COMMON_OBJ) dcg.$(CMA) ipp.$(CMA) $(SQUALL_OBJ) sparql2squall.$(CMO)

interpreter: $(SQUALL_OBJ) main.$(CMO)
	$(OCAMLC) $(INCLUDES) -o squall.exe $(COMMON_OBJ) dcg.$(CMA) ipp.$(CMA) $(SQUALL_OBJ) main.$(CMO)

webform: webform.ml
	$(OCAMLC) $(INCLUDES) -thread -package lwt -package eliom.server -c webform.ml
	$(OCAMLC) $(INCLUDES) -a -o webform.cma common.$(CMO) lSet.$(CMO) dcg.$(CMA) ipp.$(CMA) $(SQUALL_OBJ) webform.$(CMO)

# Common rules
.SUFFIXES: .mll .ml .mli .$(CMO) .cmi

%.$(CMO): %.ml
	$(OCAMLC) $(OCAMLFLAGS) -c $<

# Clean up
clean:
	rm -f *.cm[ioax]
	rm -f *.o

cleanall: clean
	cd lib && make clean
	cd lib/dcg && make clean
	cd lib/ipp && make clean

lisfs2008:
	install -o ocsigen -g ocsigen -m 644 webform.cma /var/lib/ocsigenserver/
	install -o ocsigen -g ocsigen -m 644 webform.css /var/lib/ocsigenserver/static/
	install -o ocsigen -g ocsigen -m 644 images/* /var/lib/ocsigenserver/static/images/

