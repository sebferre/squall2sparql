(*
    This file is part of 'squall2sparql' <http://www.irisa.fr/LIS/softwares/squall/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Semantics

let skip = Syntax.skip
(*let ws = Syntax.ws*)
let kwd = Syntax.kwd
let kwds = Syntax.kwds

class ['data,'s1,'p1,'pn,'p2,'p2_measure,'str,'p] context =
  object (self)
    inherit ['data,'s1,'p1,'pn,'p2,'p2_measure,'str,'p] Syntax.context as super

    method parse_e_np : 's1 = super#parse_e_np
       
    method parse_p1_noun : 'p1 = dcg
      [ _ = kwds ["URI"; "uri"] -> pred1 (`Appl "isURI")
      | _ = kwds ["IRI"; "iri"] -> pred1 (`Appl "isIRI")
      | _ = kwd "blank"; _ = kwd "node" -> pred1 (`Appl "isBLANK")
      | _ = kwd "literal" -> pred1 (`Appl "isLITERAL")
      | _ = kwd "number" -> pred1 (`Appl "isNumeric")
      | p1 = super#parse_p1_noun -> p1 ]
    method parse_p1_adj : 'p1 = dcg
      [ _ = kwd "blank" -> pred1 (`Appl "isBLANK")
      | _ = kwd "numeric" -> pred1 (`Appl "isNumeric")
      | p1 = super#parse_p1_adj -> p1 ]
    method parse_p1_verb : 'p1 = super#parse_p1_verb
    method parse_p1_imp : 'p1 = dcg
      [ _ = kwd "load" -> (fun x -> pop_arg_opt "into" (function None -> proc `Load [x] | Some y -> proc `Load [x;y]))
      | _ = kwd "clear" -> proc1 `Clear
      | _ = kwd "drop" -> proc1 `Drop
      | _ = kwd "create" -> proc1 `Create
      | _ = kwd "add" -> (fun x -> pop_arg "to" "'add' expects the preposition 'to'" (fun y -> proc `Add [x;y]))
      | _ = kwd "move" -> (fun x -> pop_arg "to" "'move' expects the preposition 'to'" (fun y -> proc `Move [x;y]))
      | _ = kwd "copy" -> (fun x -> pop_arg "to" "'copy' expects the preposition 'to'" (fun y -> proc `Copy [x;y])) ]
    method parse_pn_imp : 'pn = dcg
      [ _ = kwd "describe" -> proc `Describe
      | pn = super#parse_pn_imp -> pn ]

    method parse_p2_noun : 'p2 = dcg
      [ _ = kwd "prefix" -> pred2 (`Appl "strStarts")
      | _ = kwd "suffix" -> pred2 (`Appl "strEnds")
      | _ = kwd "substring" -> pred2 (`Appl "contains")
      | _ = kwd "pattern" -> pred2 (`Appl "REGEX")
      | _ = kwds ["str"; "string"; "strings"] -> func1 (`Appl "str")
      | _ = kwds ["lang"; "language"; "languages"] -> func1 (`Appl "lang")
      | _ = kwds ["datatype"; "datatypes"] -> func1 (`Appl "datatype")
      | _ = kwds ["length"; "lengths"] -> func1 (`Appl "strlen")
      | _ = kwds ["ucase"; "uppercase"] -> func1 (`Appl "ucase")
      | _ = kwds ["lcase"; "lowercase"] -> func1 (`Appl "lcase")
      | _ = kwds ["year"; "years"] -> func1 (`Appl "year")
      | _ = kwds ["month"; "months"] -> func1 (`Appl "month")
      | _ = kwds ["day"; "days"] -> func1 (`Appl "day")
      | _ = kwd "hours" -> func1 (`Appl "hours")
      | _ = kwd "minutes" -> func1 (`Appl "minutes")
      | _ = kwd "seconds" -> func1 (`Appl "seconds")
      | _ = kwds ["timezone"; "timezones"] -> func1 (`Appl "timezone" )
      | _ = kwd "tz" -> func1 (`Appl "tz")
      | p2 = super#parse_p2_noun -> p2 ]
    method parse_p2_count_noun : 'p2 = super#parse_p2_count_noun
    method parse_p2_adj : 'p2 = dcg
      [ _ = kwd "starting"; _ = kwd "with" -> pred2 (`Appl "strStarts")
      | _ = kwd "ending"; _ = kwd "with" -> pred2 (`Appl "strEnds")
      | _ = kwd "containing" -> pred2 (`Appl "contains")
      | _ = kwd "matching" -> pred2 (`Appl "REGEX")
      | p2 = super#parse_p2_adj -> p2 ]
    method parse_p2_adj_measure : 'p2_measure = super#parse_p2_adj_measure
    method parse_p2_adj_comparative : 'p2_measure = super#parse_p2_adj_comparative
    method parse_p2_adj_superlative : 'p2_measure = super#parse_p2_adj_superlative
    method parse_p2_verb : 'p2 = dcg
      [ _ = skip;
	( "=" -> pred2 `Eq
        | "!=" -> pred2 `Neq
        | ">" -> pred2 `Gt
        | "<" -> pred2 `Lt
        | ">=" -> pred2 `Geq
        | "<=" -> pred2 `Leq )
      | _ = kwds ["start"; "starts"]; _ = kwd "with" -> pred2 (`Appl "strStarts")
      | _ = kwds ["end"; "ends"]; _ = kwd "with" -> pred2 (`Appl "strEnds")
      | _ = kwds ["contain"; "contains"] -> pred2 (`Appl "contains")
      | _ = kwds ["match"; "matches"] -> pred2 (`Appl "REGEX")
      | p2 = super#parse_p2_verb -> p2 ]

    method parse_addop : 'p = dcg
	[ _ = skip; "+" -> `Infix "+"
	| _ = skip; "-" -> `Infix "-"
	| _ = skip; "||" -> `Infix "||" ]
    method parse_mulop : 'p = dcg
	[ _ = skip; "*" -> `Infix "*"
	| _ = skip; "/" -> `Infix "/"
	| _ = skip; "&&" -> `Infix "&&" ]
    method parse_unop : 'p = dcg
	[ _ = skip; "+" -> `Prefix "+"
        | _ = skip; "-" -> `Prefix "-"
	| _ = skip; "!" -> `Prefix "!" ]
    method parse_nulop : 'p = dcg
        [ _ = kwd "now" -> `Appl "now"
	| _ = kwd "a"; _ = kwd "random"; _ = kwd "number" -> `Appl "RAND"
	| _ = kwd "a"; _ = kwd "new"; 
	  ( _ = kwd "node" -> `Appl "BNODE"
	  | _ = kwd "UUID"; _ = kwds ["URI"; "IRI"] -> `Appl "UUID"
	  | _ = kwd "UUID"; _ = kwd "string" -> `Appl "STRUUID" ) ]
    method parse_func : 'p = dcg
        [ _ = skip; s = match "[A-Za-z_]+[0-9]*" as "func";
	    when "unknown function" List.mem (String.lowercase_ascii s)
	      [ "str"; "lang"; "datatype"; "strlen"; "ucase"; "lcase";
		"year"; "month"; "day"; "hours"; "minutes"; "timezone"; "tz";
		"uri"; "iri"; "bnode"; "strdt"; "strlang";
		"uuid"; "struuid";
		"strlen"; "substr"; "strbefore"; "strafter"; "encode_for_uri"; "concat"; "replace";
		"abs"; "round"; "ceil"; "floor"; "rand";
		"md5"; "sha1"; "sha256"; "sha384"; "sha512";
		"BNODE"; "RAND"; "now";
		"if"; "colaesce";
	      ]
	    -> `Appl s
	| uri = Syntax.parse_uri Syntax.re_local -> `Appl uri ]

    method parse_marker : 'str = dcg
      [ _ = kwd "in" -> "in"
      | _ = kwd "from" -> "from"
      | mk = super#parse_marker -> mk ]

    method parse_context (marker : string) : 'p = dcg
        [ _ = kwds ["graph"; "graphs"]; when "'in' expected before 'graph'" marker = "in" -> `GRAPH
	| _ = kwds ["service"; "services"]; when "'from' expected before 'service'" marker = "from" -> `SERVICE ]

    method parse_aggreg_noun : 'p = dcg
	[ _ = kwd "count" -> `Appl "COUNT"
	| _ = kwds ["min"; "minimum"] -> `Appl "MIN"
	| _ = kwds ["max"; "maximum"] -> `Appl "MAX"
	| _ = kwd "sum" -> `Appl "SUM"
	| _ = kwd "average" -> `Appl "AVG"
	| _ = kwd "sample" -> `Appl "SAMPLE"
	| _ = kwds ["concat"; "concatenation"] -> `Appl "GROUP_CONCAT" ]
    method parse_aggreg_adj : 'p = dcg
(*        [ _ = kwd "number"; _ = kwd "of" -> `Appl "COUNT" *)
	[ _ = kwd "minimum" -> `Appl "MIN"
	| _ = kwd "maximum" -> `Appl "MAX"
	| _ = kwd "total" -> `Appl "SUM"
	| _ = kwd "average" -> `Appl "AVG"
	| _ = kwd "sample" -> `Appl "SAMPLE"
	| _ = kwd "concatenated" -> `Appl "GROUP_CONCAT" ]

    method is_kwd_norm s : bool =
      super#is_kwd_norm s ||
	List.mem s [
	  "add"; "average";
	  "blank"; "bound";
	  "clear"; "concat"; "concatenation"; "concatenated"; "contain"; "containing"; "contains"; "copy"; "count"; "create";
	  "datatype"; "datatypes"; "day"; "days"; "decreasing"; "defined"; "describe"; "drop";
	  "earliest"; "end"; "ending"; "ends";
	  "first";
	  "graph"; "greatest";
	  "highest"; "hours";
	  "increasing"; "IRI"; "iri";
	  "lang"; "language"; "languages"; "last"; "latest"; "lcase"; "least"; "length"; "lengths"; "lowercase"; "lowest"; "literal"; "load";
	  "match"; "matching"; "matches"; "max"; "maximum"; "min"; "minimum"; "minutes"; "month"; "months"; "move";
	  "new"; "node"; "now"; "number"; "numeric";
	  "pattern"; "prefix";
	  "random";
	  "sample"; "seconds"; "service"; "start"; "starting"; "starts"; "str"; "string"; "strings"; "substring"; "suffix"; "sum";
	  "timezone"; "timezones"; "to"; "total"; "tz";
	  "ucase"; "unbound"; "undefined"; "uppercase"; "URI"; "uri"; "UUID";
	  "with";
	  "year"; "years";
	]

    val sparql_var_cpt = 1
    method current_sparql_var = Var ("sparql" ^ string_of_int sparql_var_cpt)
    method next_sparql_var = {< sparql_var_cpt = sparql_var_cpt + 1; >}
  end


(* generation of SPARQL *)

type sparql_query =
  [ `ASK of sparql_pattern
  | `SELECT of sparql_proj list * sparql_pattern * sparql_modifier list
  | `CONSTRUCT of sparql_pattern * sparql_pattern
  | `DESCRIBE of term list * sparql_pattern
  | `LOAD of uri * uri option
  | `CLEAR of sparql_graphs
  | `DROP of sparql_graphs
  | `CREATE_GRAPH of uri
  | `ADD of uri * uri
  | `MOVE of uri * uri
  | `COPY of uri * uri
  | `INSERT_DATA of sparql_pattern
  | `DELETE_DATA of sparql_pattern
  | `MODIFY of sparql_pattern (* deleted *) * sparql_pattern (* inserted *) * sparql_pattern (* where *) ]
and sparql_graphs =
  [ `ALL
  | `NAMED
  | `DEFAULT
  | `GRAPH of uri ]
and sparql_proj =
  [ `Term of term
  | `AS of sparql_expr * term ]
and sparql_modifier = (* TODO: use a record instead *)
  [ `DISTINCT
  | `GROUP_BY of term list
  | `ORDER_BY of ([`DESC | `ASC] * sparql_expr) list
  | `LIMIT of int
  | `OFFSET of int ]
and sparql_pattern =
  [ `Triples of term * sparql_descr
  | `VALUES of term * term list
  | `BIND of sparql_expr * term
  | `FILTER of sparql_expr (* not binding *)
  | `True | `False (* not binding *)
  | `JOIN of sparql_pattern list
  | `UNION of sparql_pattern list
  | `OPTIONAL of sparql_pattern
  | `GRAPH of term * sparql_pattern
  | `SERVICE of term * sparql_pattern
  | `Subquery of sparql_query ]
and sparql_descr = (sparql_predicate * sparql_object list) list
and sparql_predicate =
  [ `Term of term
  | `Seq of sparql_predicate list
  | `Alt of sparql_predicate list
  | `Star of sparql_predicate
  | `Plus of sparql_predicate
  | `Opt of sparql_predicate
  | `Inverse of sparql_predicate ]
and sparql_object =
  [ `Term of term
  | `Block of sparql_descr ] (* [ ... ] *)
and sparql_expr =
  [ `Term of term
  | `Appl of string * sparql_expr list
  | `Infix of string * sparql_expr list
  | `Prefix of string * sparql_expr list
  | `IN of sparql_expr * sparql_expr list
  | `EXISTS of sparql_pattern
  | `NOT_EXISTS of sparql_pattern
  | `Star ]
       
let rec transform_not : f -> f * f = function
  | `Context (kind, x, `True) -> transform_not `True
  | `Context (kind, x, `And lf1) ->
      let i, d = transform_not (`And lf1) in
      apply_context kind x i, apply_context kind x d
  | `Context (kind, x, `Not f1) ->
      let i, d = transform_not (`Not f1) in
      apply_context kind x i, apply_context kind x d
  | `True -> `True, `True
  | `And lf1 ->
      let li, ld = List.split (List.map transform_not lf1) in
      and_ li, and_ ld
  | `Not f1 -> `True, transform_quads f1
  | f -> transform_quads f, `True
and transform_quads : f -> f = function
  | `True -> `True
  | `And lf1 -> and_ (List.map transform_quads lf1)
  | `Exists (xs,f1) -> `Exists (xs, transform_quads f1)
  | `Context (kind,x,f1) ->
      let i = transform_triples f1 in
      apply_context kind x i
  | f -> transform_triples f
and apply_context kind x : f -> f = function
  | `True -> `True
  | f -> `Context (kind, x, f)
and transform_triples : f -> f = function
  | `True -> `True
  | `And lf1 -> and_ (List.map transform_triples lf1)
  | `Exists (xs,f1) -> `Exists (xs, transform_triples f1)
  | `Triple _ as f -> f

  | `Func _
  | `Pred _
  | `Proc _
  | `Aggreg _ -> failwith "built-ins and aggregations are not allowed in updates"
  | `Modif _ -> failwith "solution modifiers are not allowed in updates"
  | `Context _ -> failwith "nested graphs and services are not allowed in updates"
  | `Forall _ -> failwith "nested universals are not allowed in updates"
  | `Not _ -> failwith "nested negations are not allowed in updates"
  | `Or _ -> failwith "disjunctions are not allowed in updates"
  | `Option _ -> failwith "optionals are not allowed in updates"
  | _ -> failwith "invalid update"


let sparql_t x : [> `Term of term ] = `Term x

let rec sparql : f -> sparql_query list = function
  | `True -> []
  | `And lf -> List.flatten (List.map sparql lf)
  | `Select (xs,f) -> [sparql_q xs f]
  | `Proc _ as f -> sparql (`Forall (`True, f))
  | `Forall (f, `Proc (`Return, [GraphLiteral (_lv,lt)])) ->
     [`CONSTRUCT (`JOIN (List.map (fun (s,p,o) -> `Triples (s, [`Term p, [`Term o]])) lt),
                  sparql_p f)]
  | `Forall (f1, `Proc (`Return, lx)) ->
      ( match lx, f1 with
	| [x1], `Truth (x2,f2) when x1=x2 -> sparql (`Select ([],f2))
	| _ -> sparql (`Select (lx,f1)) )
  | `Forall (f, `Proc (`Describe, xs)) ->
     [`DESCRIBE (xs, sparql_p f)]
  | `Forall (`True, `Proc (`Load, [Uri x])) -> [`LOAD (x, None)]
  | `Forall (`True, `Proc (`Load, [Uri x; Uri y])) -> [`LOAD (x, Some y)]
  | `Forall (`True, `Proc (`Clear, [Uri x])) -> [`CLEAR (sparql_graphs x)] (* missing NAMED, ALL *)
  | `Forall (`True, `Proc (`Drop, [Uri x])) -> [`DROP (sparql_graphs x)] (* idem *)
  | `Forall (`True, `Proc (`Create, [Uri x])) -> [`CREATE_GRAPH x]
  | `Forall (`True, `Proc (`Add, [Uri x; Uri y])) -> [`ADD (x,y)]
  | `Forall (`True, `Proc (`Move, [Uri x; Uri y])) -> [`MOVE (x,y)]
  | `Forall (`True, `Proc (`Copy, [Uri x; Uri y])) -> [`COPY (x,y)]
  | `Forall (_, `Proc _) -> invalid_arg "This procedure call is not translatable to SPARQL"
  | `Forall (f1,f2) ->
     let i, d = transform_not f2 in
     [`MODIFY (sparql_p d, sparql_p i, sparql_p f1)]
  | f ->
     ( match transform_not f with
       | `True, `True -> []
       | `True, d -> [`DELETE_DATA (sparql_p d)]
       | i, `True -> [`INSERT_DATA (sparql_p i)]
       | i, d -> [`INSERT_DATA (sparql_p i); `DELETE_DATA (sparql_p d)] )
and sparql_graphs : string -> sparql_graphs = function
  | "ALL" -> `ALL
  | "NAMED" -> `NAMED
  | "DEFAULT" -> `DEFAULT
  | uri -> `GRAPH uri
and sparql_q (xs : term list) (f : f) : sparql_query =
  match xs, f with
  | [], _ ->
     `ASK (sparql_p f)
  | _, `Exists (_, f1) -> sparql_q xs f1
  | _, `Modif (`Mod { order; offset; limit }, _lz, x, f1) ->
     let vars = if List.mem x xs then xs else xs@[x] in
     let modifiers = [] in
     let modifiers =
       if offset = 0 then modifiers
       else `OFFSET offset :: modifiers in
     let modifiers =
       if limit < 0 then modifiers
       else `LIMIT limit :: modifiers in
     let modifiers =
       match order with
       | `NONE -> modifiers
       | (`DESC | `ASC as order) -> `ORDER_BY [(order, `Term x)] :: modifiers in
     let modifiers = `DISTINCT :: modifiers in
     `SELECT (List.map sparql_t vars, sparql_p f1, modifiers)
  | _, `Aggreg (op, x, lz, f1) ->
     let aggreg =
       match op with
       | `AggregOp0 `Count -> `Appl ("COUNT", [`Star])
       | `AggregOp1 (`Count, y) -> `Appl ("COUNT", [`Prefix ("DISTINCT ", [`Term y])])
       | `AggregOp1 (`Appl "COUNT", y) -> `Appl ("COUNT", [`Prefix ("DISTINCT ", [`Term y])])
       | `AggregOp1 (`Appl g, y) -> `Appl (g, [`Term y])
       | _ -> assert false (* invalid aggreg operator *) in
     let modifiers = [`GROUP_BY lz] in
     `SELECT (List.map sparql_t lz @ [ `AS (aggreg, x) ], sparql_p f1, modifiers)
  | _ ->
     `SELECT (List.map sparql_t xs, sparql_p f, [`DISTINCT])
and sparql_p : f -> sparql_pattern = function
  | `Triple (s, Uri "rdf:element", o) -> sparql_p (`Triple (s, Uri "rdf:rest*/rdf:first", o))
  | `Triple (s, Uri "rdf:last", o) -> sparql_p (`Triple (s, Uri "rdf:rest*", o))
  | `Triple (s,p,o) -> `Triples (s, [`Term p, [`Term o]])
  | `Matches (x,lre) ->
     let le =
       List.map
         (fun re ->
           `Appl ("REGEX", [`Appl ("str", [`Term x]);
                            `Term (Literal ("\"" ^ String.escaped re ^ "\"", Bare));
                            `Term (Literal ("'i'", Bare))]))
         lre in
     let e =
       match le with
       | [] -> assert false
       | [e1] -> e1
       | _ -> `Infix ("&&", le) in
     `FILTER e
  | `Func (`Id, [Uri _ as x], (Var _ as y)) -> `VALUES (y, [x])
  | `Func (`Id, [x], (Var _ as y)) -> `BIND (`Term x, y)
  | `Func (`Id, [Cons (e,l)], y) -> `JOIN [`Triples (y, [`Term (Uri "rdf:first"), [`Term e]; `Term (Uri "rdf:rest"), [`Term l]])]
  | `Func (op, lx, (Var _ as y)) -> `BIND (sparql_func op (List.map sparql_t lx), y)
  | `Func (op, lx, y) -> `FILTER (`Infix ("=", [sparql_func op (List.map sparql_t lx); `Term y]))
  | `Pred (`Eq, [(Uri _ as x); (Var _ as y)]) -> `VALUES (y, [x])
  | `Pred (`Eq, [x; (Var _ as y)]) -> `BIND (`Term x, y)
  | `Pred ((`Eq|`Neq|`Geq|`Gt|`Leq|`Lt as op), [(Var _ as x); (Literal (s,ltyp) as l)]) ->
     let e1, ltyp =
       match ltyp with
       | Datatype ("xsd:boolean"|"xsd:int"|"xsd:integer"|"xsd:decimal"|"xsd:double" as dt) ->
          sparql_func (`Appl dt) [sparql_func (`Appl "str") [sparql_t x]], ltyp
       | Bare | Datatype _ -> sparql_func (`Appl "str") [sparql_t x], Bare
       | Lang _ -> sparql_t x, ltyp in
     let e2 = sparql_t (Literal (s,ltyp)) in
     `FILTER (sparql_pred op [e1; e2])
  | `Pred (op, lx) -> `FILTER (sparql_pred op (List.map sparql_t lx))
  | `Truth (x,f) -> `BIND (`EXISTS (sparql_p f), x)
  | `Context (`GRAPH,x,f) -> `GRAPH (x, sparql_p f)
  | `Context (`SERVICE,x,f) -> `SERVICE (x, sparql_p f)
  | `Context _ -> assert false (* invalid context op *)
  | `Aggreg (op,x,lz,f1) as f -> `Subquery (sparql_q (x::lz) f)
  | `Modif (op,lz,x,f1) as f -> `Subquery (sparql_q (x::lz) f)
  | `Exists (xs,f) -> sparql_p f
  | `Forall (f1,f2) -> sparql_p (`Not (`And [f1; `Not f2]))
  | `Cond (c,f1,f2) -> failwith "Conditionals are not supported in SPARQL"
  | `True -> `True
  | `Not f ->
     (match sparql_p f with
      | `True -> `False
      | `False -> `True
      | `FILTER e -> `FILTER (`Prefix ("!", [e]))
      | f' -> `FILTER (`NOT_EXISTS f'))
  | `And lf ->
     let lf' = List.map sparql_p lf in
     if List.mem `False lf' then `False
     else
       let lf' = List.filter ((<>) `True) lf' in
       if List.for_all (function `FILTER _ -> true | _ -> false) lf'
       then `FILTER
              (`Infix ("&&",
                       List.map
                         (function `FILTER e -> e | _ -> assert false)
                         lf'))
       else `JOIN lf'
  | `Or lf ->
     let lf' = List.map sparql_p lf in
     let opt, res =
       List.fold_right
         (fun f1 (opt,res) ->
           match f1, res with
           | `False, _ -> opt, res
           | `True, _ -> true, res
           | _, `FILTER e2 -> (* filter is contaminating, not reducible to binding pattern *)
              let e1 =
                match f1 with
                | `FILTER e1 -> e1
                | `BIND (e1,x) -> `Infix ("=", [`Term x; e1])
                | `VALUES (x,ly) -> `IN (`Term x, List.map (fun y -> `Term y) ly)
                | _ -> `EXISTS f1 in (* not optimal *)
              opt, `FILTER (`Infix ("||", [e1; e2]))
           | `VALUES (x1,l1), `VALUES (x2,l2) when x1=x2 ->
              opt, `VALUES (x1, l1 @ l2)
           | _, `False -> opt, f1
           | `UNION lf1, `UNION lf2 -> opt, `UNION (lf1 @ lf2)
           | _, `UNION lf2 -> opt, `UNION (f1 :: lf2)
           | `UNION lf1, _ -> opt, `UNION (lf1 @ [res])
           | _ -> opt, `UNION [f1; res])
         lf' (false, `False) in
     if opt then `OPTIONAL res else res       
  | `Option f ->
     ( match sparql_p f with
       | `True -> `True
       | `False -> `True
       | `FILTER _ -> `True
       | f' -> `OPTIONAL f' )
  | `Label _
    | `Proc _
    | `Select _
    | `Unify _ -> assert false (* should be eliminated by [transform] *)
and sparql_func op le : sparql_expr =
  match op, le with
  | `Id, [e] -> e
  | `Id, _ -> assert false
  | `Prefix f, le -> `Prefix (f, le)
  | `Infix f, le -> `Infix (f, le)
  | `Appl f, le -> `Appl (f, le)
  | _ -> assert false (* should be eliminated by [transform] *)
and sparql_pred op le : sparql_expr =
  match op, le with
  | `Appl f, le -> `Appl (f, le)
  | `Infix f, le -> `Infix (f, le)
  | `Eq, le -> `Infix ("=", le)
  | `Neq, le -> `Infix ("!=", le)
  | `Geq, le -> `Infix (">=", le)
  | `Leq, le -> `Infix ("<=", le)
  | `Gt, le -> `Infix (">", le)
  | `Lt, le -> `Infix ("<", le)
  | _ -> assert false (* should be eliminated by [transform] *)

let of_semantics sem : sparql_query list = sparql sem

                                         
(* printing SPARQL *)
  
let space = ipp [ _ -> " " ]
let dot = ipp [ _ -> " . " ]
let print_int = ipp [ i -> '(string_of_int i) ]
let print_literal_type = ipp
    [ Bare -> 
    | Datatype dt -> "^^"; 'dt
    | Lang lang -> "@"; 'lang ]
let print_literal = ipp
    [ s, Datatype "xsd:boolean" -> 's
    | s, Datatype ("xsd:integer" | "xsd:int") -> 's
    | s, Datatype "xsd:decimal" -> 's
    | s, Datatype "xsd:double" -> 's
    | s, ltyp -> "\""; 's; "\""; print_literal_type of ltyp ]
let rec print_term = ipp
    [ Var "" -> "[]"
    | Var s -> "?"; 's
    | Ref l -> '(failwith "some references are unresolved")
    | Uri s -> 's
    | Literal (s,ltyp) -> print_literal of s, ltyp
    | GraphLiteral _ -> '(failwith "graph literals are not supported in SPARQL")
    | Cons (e,l) -> "[ rdf:first "; print_term of e; " ; rdf:rest "; print_term of l; " ]" ]

let rec print = ipp
    [ lq -> LIST0 print_query SEP "\n" of lq; EOF ]
and print_query : sparql_query -> _ = ipp
    [ `ASK p -> "ASK { "; print_pattern of p; "}"
    | `SELECT (xs, p, modifs) -> "SELECT DISTINCT "; LIST0 print_proj SEP " " of xs; " WHERE { "; print_pattern of p; "}"; MANY print_modifier of modifs
    | `CONSTRUCT (p1, p2) -> "CONSTRUCT { "; print_pattern of p1; "} WHERE { "; print_pattern of p2; "}"
    | `DESCRIBE (xs, p) -> "DESCRIBE "; LIST1 print_term SEP " " of xs; [ `True ->  | p -> " WHERE { "; print_pattern of p; "}" ] of p
    | `LOAD (x,None) -> "LOAD "; 'x
    | `LOAD (x, Some y) -> "LOAD "; 'x; " INTO GRAPH "; 'y
    | `CLEAR x -> "CLEAR "; print_graphs of x
    | `DROP x -> "DROP "; print_graphs of x
    | `CREATE_GRAPH x -> "CREATE GRAPH "; 'x
    | `ADD (x,y) -> "ADD "; 'x; " TO "; 'y
    | `MOVE (x,y) -> "MOVE "; 'x; " TO "; 'y
    | `COPY (x,y) -> "COPY "; 'x; " TO "; 'y
    | `INSERT_DATA i -> "INSERT DATA { "; print_pattern of i; "} "
    | `DELETE_DATA d -> "DELETE DATA { "; print_pattern of d; "} "
    | `MODIFY (d,i,g) ->
	[ d -> when d <> `True; "DELETE { "; print_pattern of d; "} " | _ -> ] of d;
	[ i -> when i <> `True; "INSERT { "; print_pattern of i; "} " | _ -> ] of i;
	[ g -> when g <> `True; "WHERE { "; print_pattern of g; "}" | _ -> ] of g
    | _ -> "..." ]
and print_graphs : sparql_graphs -> _ = ipp
    [ `ALL -> "ALL"
    | `NAMED -> "NAMED"
    | `DEFAULT -> "DEFAULT"
    | `GRAPH uri -> "GRAPH "; 'uri
    | _ -> "..." ]
and print_proj : sparql_proj -> _ = ipp
    [ `Term x -> print_term of x
    | `AS (e,x) -> "("; print_expr of e; " AS "; print_term of x; ")"
    | _ -> "..." ]
and print_modifier : sparql_modifier -> _ = ipp
    [ `DISTINCT ->
    | `GROUP_BY [] -> 
    | `GROUP_BY xs -> " GROUP BY "; LIST1 print_term SEP " " of xs
    | `ORDER_BY l_dir_e -> " ORDER BY ";
                           LIST1 [ `DESC, e -> "DESC("; print_expr of e; ")"
                                 | `ASC, e -> print_expr of e ] SEP " " of l_dir_e
    | `LIMIT n -> " LIMIT "; print_int of n
    | `OFFSET n -> " OFFSET "; print_int of n
    | _ -> "..." ]
and print_pattern : sparql_pattern -> _ = ipp
    [ `Triples (s,lpo) -> print_term of s; space; print_descr of lpo; dot
    | `VALUES (x,ly) -> "VALUES "; print_term of x; " { "; LIST1 print_term SEP " " of ly; " } "
    | `BIND (e,x) -> "BIND ("; print_expr of e; " AS "; print_term of x; ") "
    | `FILTER (`EXISTS p) -> "FILTER EXISTS { "; print_pattern of p; "} "
    | `FILTER (`NOT_EXISTS p) -> "FILTER NOT EXISTS { "; print_pattern of p; "} "
    | `FILTER e -> "FILTER ("; print_expr of e; ") "
    | `True ->
    | `False -> '(failwith "The SPARQL query cannot succeed, it will always fail")
    | `JOIN lp -> MANY print_pattern of lp
    | `UNION lp -> LIST1 [ p -> "{ "; print_pattern of p; "} " ] SEP "UNION " of lp
    | `OPTIONAL p -> "OPTIONAL { "; print_pattern of p; "} "
    | `GRAPH (x,p) -> "GRAPH "; print_term of x; " { "; print_pattern of p; "} "
    | `SERVICE (x,p) -> "SERVICE "; print_term of x; " { "; print_pattern of p; "} "
    | `Subquery q -> "{ "; print_query of q; " } "
    | _ -> "..." ]
and print_descr : sparql_descr -> _ = ipp
    [ lpo -> LIST1 [ (p,lo) -> print_predicate of p; " "; LIST1 print_object SEP " , " of lo ] SEP " ; " of lpo ]
and print_predicate : sparql_predicate -> _ = ipp (* TODO: manage priorities *)
    [ `Term p -> print_term of p
    | `Seq lp -> LIST1 print_predicate SEP "/" of lp
    | `Alt lp -> "("; LIST1 print_predicate SEP "|" of lp
    | `Star p -> print_predicate of p; "*"
    | `Plus p -> print_predicate of p; "+"
    | `Opt p -> print_predicate of p; "?"
    | `Inverse p -> "^"; print_predicate of p ]
and print_object : sparql_object -> _ = ipp
    [ `Term o -> print_term of o
    | `Block lpo -> "[ "; print_descr of lpo; " ]"
    | _ -> "..." ]
and print_expr : sparql_expr -> _ = ipp
    [ `Term x -> print_term of x
    | `Appl (f, le) -> 'f; "("; LIST0 print_expr SEP ", " of le; ")"
    | `Infix (f, le) -> let sep = " " ^ f ^ " " in "("; LIST1 print_expr SEP 'sep of le; ")"
    | `Prefix (f, le) -> 'f; LIST0 print_expr SEP " " of le
    | `IN (e1,le2) -> print_expr of e1; " IN ("; LIST0 print_expr SEP ", " of le2; ")"
    | `EXISTS p -> "EXISTS { "; print_pattern of p; " }"
    | `NOT_EXISTS p -> "NOT EXISTS { "; print_pattern of p; " }"
    | `Star  -> "*"
    | _ -> "..." ]


let skip = dcg "skip" [ s = match "[ \t\r\n]*" as "skip" -> s ]
let ws = dcg "space" [ s = match "[ \t\r\n]+" as "space" -> s ]
let ident = dcg "ident" [ s = match "[A-Za-z_]+" as "ident" -> s ]
let nat = dcg "nat" [ s = match "\\(0\\|[1-9][0-9]*\\)" as "natural number" -> int_of_string s ]

let kw (s_upper : string) = dcg "kwd"
    [ s = match "[A-Za-z]+" as "kwd"; when "unexpected SPARQL kwd" (String.uppercase_ascii s = s_upper) -> () ]
let op (s : string) = dcg "op" [ _ = skip; 's; _ = skip -> () ]
                    
let comma = dcg "comma" [ _ = skip; ","; _ = skip -> () ]
let semicolon = dcg "semicolon" [ _ = skip; ";"; _ = skip -> () ]
let dot = dcg "dot" [ _ = skip; "."; _ = skip -> () ]
let pipe = dcg "pipe" [ _ = skip; "|"; _ = skip -> () ]
let open_curly = dcg "open curly" [ _ = skip; "{"; _ = skip -> () ]
let close_curly = dcg "close curly" [ _ = skip; "}"; _ = skip -> () ]


let parse_curly parse_contents = dcg "curly"
    [ "{"; _ = skip; x = parse_contents; _ = skip; "}" -> x ]
        
let rec parse : (_, sparql_query, _) Dcg.parse = dcg
    [ q = parse_q; _ = skip; EOF -> q ]
and parse_q = dcg " query"
    [ _ = kw "ASK"; _ = skip; "{"; _ = skip;
      p = parse_p; _ = skip; "}" -> `ASK p
    | q = parse_q_select -> q ] (* TODO: other query forms, including updates *)
and parse_q_select = dcg "query_select"
    [ _ = kw "SELECT"; _ = ws;
      modifs1 = OPT [ _ = kw "DISTINCT"; _ = ws -> [`DISTINCT] ] ELSE [];
      projs = LIST1 parse_proj SEP ws; _ = ws;
      _ = kw "WHERE"; _ = open_curly; p = OPT parse_p ELSE `True; _ = close_curly;
      modifs2 = LIST0 parse_modif SEP ws -> `SELECT (projs, p, modifs1 @ modifs2) ]
and parse_proj = dcg "proj"
    [ t = parse_term -> `Term t
    | "("; _ = skip; e = parse_expr; _ = ws;
      _ = kw "AS"; _ = ws; t = parse_term; _ = skip; ")" -> `AS (e,t) ]
and parse_modif = dcg "modif"
    [ _ = kw "GROUP"; _ = ws; _ = kw "BY"; _ = ws;
      lt = LIST1 parse_term SEP ws -> `GROUP_BY lt
    | _ = kw "ORDER"; _ = ws; _ = kw "BY"; _ = ws; l_dir_e = LIST1 parse_order SEP ws -> `ORDER_BY l_dir_e
    | _ = kw "LIMIT"; _ = ws; n = nat -> `LIMIT n
    | _ = kw "OFFSET"; _ = ws; n = nat -> `OFFSET n ]
and parse_order = dcg "order"
    [ _ = kw "ASC"; _ = skip; "("; _ = skip; e = parse_expr; _ = skip; ")" -> `ASC, e
    | _ = kw "DESC"; _ = skip; "("; _ = skip; e = parse_expr; _ = skip; ")" -> `DESC, e
    | e = parse_expr -> `ASC, e ]
and parse_p = dcg "pattern"
    [ lp = LIST1 parse_p_atom SEP skip -> `JOIN lp ]
and parse_p_atom = dcg "pattern_atom"
    [ s = parse_term; _ = ws; lpo = parse_descr; _ = dot -> `Triples (s,lpo)
    | _ = kw "VALUES"; _ = ws; x = parse_term; _ = ws; "{"; _ = skip;
      lt = LIST1 parse_term SEP ws; _ = skip; "}" -> `VALUES (x,lt)
    | _ = kw "BIND"; _ = skip; "("; _ = skip; e = parse_expr; _ = ws;
      _ = kw "AS"; _ = ws; x = parse_term; _ = skip; ")" -> `BIND (e,x)
    | _ = kw "FILTER"; _ = skip; e = parse_filter -> `FILTER e
    | _ = kw "OPTIONAL"; _ = skip; "{"; _ = skip; p = parse_p; _ = skip; "}" -> `OPTIONAL p
    | _ = kw "MINUS"; _ = skip; p = parse_curly parse_p -> `FILTER (`NOT_EXISTS p)
    | _ = kw "GRAPH"; _ = ws; g = parse_term; _ = ws; p = parse_curly parse_p -> `GRAPH (g,p)
    | _ = kw "SERVICE"; _ = ws; s = parse_term; _ = ws; p = parse_curly parse_p -> `SERVICE (s,p)
    | lp = LIST1 parse_curly parse_p SEP [ _ = skip; _ = kw "UNION"; _ = skip -> () ] -> `UNION lp
    | q = parse_curly parse_q_select -> `Subquery q ]
and parse_descr = dcg "descr"
    [ lpo = LIST1 [ p = parse_predicate; _ = ws; lo = LIST1 parse_object SEP comma -> (p,lo) ] SEP semicolon -> lpo ]
and parse_predicate = dcg "predicate"
    [ v = parse_var -> `Term (Var v)
    | p = parse_predicate_alt -> p ]
and parse_predicate_alt = dcg "predicate/alt"
    [ lp = LIST1 parse_predicate_seq SEP [ _ = skip; "|"; _ = skip -> () ] ->
      (match lp with
       | [] -> assert false
       | [p] -> p
       | _ -> `Alt lp) ]              
and parse_predicate_seq = dcg "predicate/seq"
    [ lp = LIST1 parse_predicate_inverse SEP "/" ->
      (match lp with
       | [] -> assert false
       | [p] -> p
       | _ -> `Seq lp) ]
and parse_predicate_inverse = dcg "predicate/inverse"
    [ "^"; _ = skip; p = parse_predicate_suffix -> `Inverse p
    | p = parse_predicate_suffix -> p ]
and parse_predicate_suffix = dcg "predicate/suffix"
    [ p = parse_predicate_atom;
      ( "*" -> `Star p
      | "+" -> `Plus p
      | "?" -> `Opt p
      | -> p ) ]
and parse_predicate_atom = dcg "predicate_atom" (* TODO: better manage priorities *)
    [ uri = parse_uri -> `Term (Uri uri)
    | "a" -> `Term (Uri "rdf:type")
    | "("; _ = skip; lp = LIST1 parse_predicate_alt SEP pipe; _ = skip; ")" -> `Alt lp ]
and parse_object = dcg "object"
    [ o = parse_term -> `Term o
    | "["; lpo = OPT [ _ = skip; lpo = parse_descr -> lpo ] ELSE []; _ = skip; "]" -> `Block lpo ]
and parse_filter = dcg "filter"
    [ "("; _ = skip; e = parse_expr; _ = skip; ")" -> e
    | _ = kw "EXISTS"; _ = skip; "{"; _ = skip; p = parse_p; _ = skip; "}" -> `EXISTS p
    | _ = kw "NOT"; _ = ws; _ = kw "EXISTS"; _ = skip; "{"; _ = skip; p = parse_p; _ = skip; "}" -> `NOT_EXISTS p ]
and parse_expr = dcg "expr/or"
    [ le = LIST1 parse_expr_and SEP [ _ = op "||" -> ()] ->
      (match le with [e] -> e | _ -> `Infix ("||", le)) ]
and parse_expr_and = dcg "expr/and"
    [ le = LIST1 parse_expr_comp SEP [ _ = op "&&" -> ()] ->
      (match le with [e] -> e | _ -> `Infix ("&&", le)) ]
and parse_expr_comp = dcg "expr/comp"
    [ e1 = parse_expr_add;
      ( _ = skip; op = match "\\(!?=\\|<=?\\|>=?\\)" as "comp_op"; _ = skip;
        e2 = parse_expr_add -> `Infix (op, [e1;e2])
      | _ = ws; _ = kw "IN"; _ = skip; "("; 
        le2 = LIST1 parse_expr SEP comma; _ = skip; ")" -> `IN (e1,le2)
      | _ = ws; _ = kw "NOT"; _ = ws; _ = kw "IN"; _ = skip; "("; 
        le2 = LIST1 parse_expr SEP comma; _ = skip; ")" -> `Prefix ("!", [`IN (e1,le2)])
      | -> e1 ) ]
and parse_expr_add = dcg "expr/add"
    [ e1 = parse_expr_mult; e = parse_expr_add_aux e1 -> e ]
and parse_expr_add_aux e1 = dcg "expr/add/aux"
    [ _ = op "+"; e2 = parse_expr_mult -> `Infix ("+", [e1;e2])
    | _ = op "-"; e2 = parse_expr_mult -> `Infix ("-", [e1;e2])
    |  -> e1 ]
and parse_expr_mult = dcg "expr/mult"
    [ e1 = parse_expr_unary; e = parse_expr_mult_aux e1 -> e ]
and parse_expr_mult_aux e1 = dcg "expr/mult/aux"
    [ _ = op "*"; e2 = parse_expr_unary -> `Infix ("*", [e1;e2])
    | _ = op "/"; e2 = parse_expr_unary -> `Infix ("/", [e1;e2])
    |  -> e1 ]
and parse_expr_unary = dcg "expr/unary"
    [ "!"; _ = skip; e1 = parse_expr_atom -> `Prefix ("!", [e1])
    | "+"; _ = skip; e1 = parse_expr_atom -> `Prefix ("+", [e1])
    | "-"; _ = skip; e1 = parse_expr_atom -> `Prefix ("-", [e1])
    | e1 = parse_expr_atom -> e1 ]
and parse_expr_atom = dcg "expr/atom"
    [ t = parse_term -> `Term t
    | g = parse_aggreg; "("; distinct = OPT [ _ = skip; _ = kw "DISTINCT"; _ = ws -> true ] ELSE false; e = parse_expr; _ = skip; ")" -> `Appl (g, [e]) (* TODO: handle DISTINCT, and extra args *)
    | f = parse_func; "("; le = LIST0 parse_expr SEP comma; ")" -> `Appl (f,le)
    | _ = kw "EXISTS"; _ = skip; "{"; _ = skip; p = parse_p; _ = skip; "}" -> `EXISTS p
    | _ = kw "NOT"; _ = ws; _ = kw "EXISTS"; _ = skip; "{"; _ = skip; p = parse_p; _ = skip; "}" -> `NOT_EXISTS p
    | "*" -> `Star
    | "("; _ = skip; e = parse_expr; _ = skip; ")" -> e ]
and parse_aggreg = dcg "aggreg"
    [ _ = kw "COUNT" -> "COUNT"
    | _ = kw "SUM" -> "SUM"
    | _ = kw "AVG" -> "AVG"
    | _ = kw "MAX" -> "MAX"
    | _ = kw "MIN" -> "MIN"
    | _ = kw "SAMPLE" -> "SAMPLE" ]
and parse_func = dcg "func"
    [ id = ident -> String.uppercase_ascii id
    | uri = parse_uri -> uri ]
and parse_term = dcg "term"
    [ uri = parse_uri -> Uri uri
    | s, ltyp = parse_literal -> Literal (s,ltyp)
    | v = parse_var -> Var v ]
and parse_uri = dcg "uri"
    [ uri = match Syntax.re_reluri as "(relative) URI" -> uri
    | qname = match Syntax.re_qname as "qualified name" -> qname ]
and parse_literal = dcg "literal"
    [ "true" -> "true", Datatype "xsd:boolean"
    | "false" -> "false", Datatype "xsd:boolean"
    | s = match "[+-]?\\([0-9]+\\(\\.[0-9]*\\)?\\|\\.[0-9]+\\)[eE][+-]?[0-9]+" as "double" -> s, Datatype "xsd:double"
    | s = match "[+-]?[0-9]*\\.[0-9]+" as "decimal" -> s, Datatype "xsd:decimal"
    | s = match "[+-]?[0-9]+" as "integer" -> s, Datatype "xsd:integer"
    | s = parse_string;
      ( "@"; lang = match "[-a-zA-Z0-9]+" as "language tag" -> s, Lang lang
      | "^^"; uri = parse_uri -> s, Datatype uri
      |  -> s, Bare ) ]
and parse_string = dcg "string"
    [ "\""; s = match "\\([^\"\\\n\r]\\|[\\][tbnrf\"]\\)*" as "string"; "\"" -> s
    | "'"; s = match "\\([^'\\\n\r]\\|[\\][tbnrf\"]\\)*" as "single-quote string"; "'" -> s
    | "\"\"\""; s = match "\\(\\(\"\\|\"\"\\)?\\([^\"\\]\\|[\\][tbnrf\"]\\)\\)*" as "long string"; "\"\"\"" -> s ]
and parse_var = dcg "var"
    [ "?"; v = match "[A-Za-z_][A-Za-z_0-9]*" as "variable" -> v ]
