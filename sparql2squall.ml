(*
    This file is part of 'squall2sparql' <http://www.irisa.fr/LIS/softwares/squall/>

    Sébastien Ferré <ferre@irisa.fr>, équipe LIS, IRISA/Université Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(*
   Command line version of backward translation, from SPARQL to SQUALL.
   Usage: read a SPARQL query as arg or stdin, and write a SQUALL sentence on stdout.
*)

let _ =
  try
    let sparql_opt = ref None in
    Arg.parse [
        "-wikidata", Arg.Unit (fun () -> ()), "target Wikidata schema (P31 for rdf:type, and reified statements)"; (* already set in semantics.ml *)
      ]
      (fun s -> sparql_opt := Some s)
      "./sparql2squall [-wikidata] [sparql]";
    let context = new Sparql.context (*Dbpedia.context*) in
    let _, ast_sparql =
      let cursor_parse =
        match !sparql_opt with
        | Some sparql -> Matcher.cursor_of_string sparql
        | None -> Matcher.cursor_of_channel stdin in
      Dcg.once Sparql.parse () cursor_parse in
    let sem = Backtranslation.sem ast_sparql in
    (*let _ =
      Semantics.output_sem Format.std_formatter context sem;
      prerr_newline () in *)
    try
      let ast_squall = Backtranslation.squall sem in
      let squall =
        let cursor = Printer.cursor_of_formatter (Format.str_formatter) in
        Ipp.once Backtranslation.print ast_squall cursor context;
        Format.flush_str_formatter () in
      print_endline squall;
      exit 0
    with exn ->
      prerr_string "SEM: ";
      Semantics.output_sem Format.err_formatter context sem;
      prerr_newline ();
      raise exn
  with exn ->
    prerr_string (Printexc.to_string exn);
    prerr_newline ();
    exit 1
