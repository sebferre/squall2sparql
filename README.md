To build SQUALL tools, here are the instructions:

### Clone the git repository in some directory, and its dependencies (ocaml-lib)

```
> git clone https://bitbucket.org/sebferre/squall2sparql.git
> cd squall2sparql
> git clone https://bitbucket.org/sebferre/ocaml-lib.git
> mv ocaml-lib lib
```

### Set up the OCaml environment

```
> opam switch create 4.13.1
> opam switch 4.13.1
> eval $(opam env)
> opam install ocamlfind
> opam install yojson
> opam install camlp5
> opam install num
```

### Compile from sources

```
> make
```

If successful, you get the following "user interfaces":

- script `squall2sparql`: takes a SQUALL sentence on the standard input, and returns the SPARQL translation on the standard output
- script `sparql2squall`: takes a SPARQL query on the standard input, and returns the SQUALL translation on the standard output
- interpreter `squall.exe`: prompts the user for SQUALL sentences, and displays its SPARQL translation, as well as intermediate representations in between
