
open Semantics

(* utilities *)

let partition_map (f : 'a -> 'b option) (l : 'a list) : 'b list * 'a list =
  List.fold_right
    (fun x (ly,lz) ->
      match f x with
      | Some y -> y::ly, lz
      | None -> ly, x::lz)
    l ([],[])

let rec list_last : 'a list -> 'a = function
  | [] -> assert false
  | [x] -> x
  | _::xs -> list_last xs

let rec list_prefix_of_suffix (l : 'a list) (suf : 'a list) : 'a list =
  (* assumes that suf is a physical suffix of l *)
  if l == suf
  then []
  else
    match l with
    | [] -> assert false
    | x1::l1 ->
       let pre1 = list_prefix_of_suffix l1 suf in
       x1::pre1

let typed_literal (s : string) : term =
  let dt_re_opt =
    List.find_opt
      (fun (dt,re) -> Str.string_match (Str.regexp re) s 0 && Str.matched_string s = s)
      ["xsd:boolean", Syntax.re_boolean;
       "xsd:double", Syntax.re_double;
       "xsd:decimal", Syntax.re_decimal;
       "xsd:integer", Syntax.re_integer;
       "xsd:date", Syntax.re_date;
       "xsd:time", Syntax.re_time;
       "xsd:dateTime", Syntax.re_dateTime] in
  match dt_re_opt with
  | Some (dt, _) -> Literal (s, Datatype dt)
  | None -> Literal (s, Bare)

    
(* from SPARQL to logical formulas *)
        
class genvar =
object
  val mutable var_cpt = 0
  method new_var =
    var_cpt <- var_cpt + 1;
    Var ("__x" ^ string_of_int var_cpt)
end

let sparql_equal x y = `Infix ("=", [x;y])
  
let rec sem_query (gv : genvar) : Sparql.sparql_query -> f = function
  | `ASK p ->
     let f = sem_pattern gv p in
     `Select ([], f)
  | `SELECT (projs, p, modifs) ->
     let f = sem_pattern gv p in
     let lt, lf, aggregs = sem_projs gv projs in
     let f = and_ (f::lf) in
     let f = sem_modifs gv f aggregs modifs in
     `Select (lt, f)
  | _ -> failwith "Only SELECT/ASK queries are supported so far" (* TODO *)
and sem_projs gv (projs : Sparql.sparql_proj list) : term list * f list * (aggreg_op * term) list =
  List.fold_right
    (fun proj (lt,lf,aggregs) ->
      match proj with
      | `Term t -> t::lt, lf, aggregs
      | `AS (e,x) ->
         (match e with
          | `Appl ("COUNT", [`Star]) ->
             x::lt, lf, (`AggregOp0 `Count, x)::aggregs
          | `Appl (("COUNT"|"SUM"|"AVG"|"MIN"|"MAX"|"SAMPLE"|"GROUP_CONCAT" as op), [e1]) ->
             let op_sem = if op = "COUNT" then `Count else `Appl op in
             let y, f1 = sem_expr gv e1 in
             x::lt, f1::lf, (`AggregOp1 (op_sem, y), x)::aggregs
          | _ -> (* not an aggregate *)
             let f = sem_filter gv (sparql_equal (`Term x) e) in
             x::lt, f::lf, aggregs))
    projs ([],[],[])
and sem_modifs gv f (aggregs : (aggreg_op * term) list) (modifs : Sparql.sparql_modifier list) : f =
  let f = (* aggregate *)
    match aggregs with
    | [] -> f
    | [(aggreg_op,x)] ->
       let lz =
         match List.find_map (function `GROUP_BY lt -> Some lt | _ -> None) modifs with
         | None -> []
         | Some lz -> lz in
       `Aggreg (aggreg_op, x, lz, f)
    | _ -> failwith "Multiple aggregates are not supported yet" (* TODO *) in
  let f = (* solution modifier: sorting + slice *)
    match List.find_map (function `ORDER_BY l_dir_e -> Some l_dir_e | _ -> None) modifs with
    | None | Some [] -> f (* ignoring LIMIT/OFFSET because no associated variable *)
    | Some ((dir,e)::_) -> (* ignoring secondary ordering criteria *)
       let x, f =
         match e with
         | `Appl (("COUNT"|"SUM"|"AVG"|"MIN"|"MAX"|"SAMPLE"|"GROUP_CONCAT" as op), [_]) ->
            failwith "Aggregates in ORDER BY are not supported yet" (* TODO *)
         | _ ->
            let x, f_e = sem_expr gv e in
            let f = and_ [f; f_e] in
            x, f in
       let order = (dir :> [`NONE|`ASC|`DESC]) in
       let offset =
         List.fold_left (fun res -> function `OFFSET n -> n | _ -> res) 0 modifs in
       let limit =
         List.fold_left (fun res -> function `LIMIT n -> n | _ -> res) (-1) modifs in
       `Modif (`Mod {order; offset; limit}, [], x, f) in
  f
and sem_pattern gv : Sparql.sparql_pattern -> f = function
  | `Triples (s,lpo) -> `And (sem_descr gv s lpo)
  | `VALUES (x,lt) -> `Or (List.map (fun t -> sem_filter gv (sparql_equal (`Term x) (`Term t))) lt)
  | `BIND (e,x) -> sem_filter gv (sparql_equal (`Term x) e)
  | `FILTER e -> sem_filter gv e
  | `True -> `True
  | `False -> assert false
  | `JOIN lp -> and_ (List.map (sem_pattern gv) lp)
  | `UNION lp -> `Or (List.map (sem_pattern gv) lp)
  | `OPTIONAL p -> `Option (sem_pattern gv p)
  | `GRAPH (t,p) -> `Context (`GRAPH, t, sem_pattern gv p)
  | `SERVICE (t,p) -> `Context (`SERVICE, t, sem_pattern gv p)
  | `Subquery q -> sem_query gv q
and sem_descr gv s lpo : f list =
  List.concat_map
    (fun (pred,lo) -> sem_objects gv s pred lo)
    lpo
and sem_objects gv s pred lo : f list =
  List.concat_map
    (fun obj ->
      let o, lfobj = sem_object gv obj in
      sem_pred gv s pred o @ lfobj)
    lo
and sem_pred gv s pred o : f list =
  match pred with
  | `Term p -> [`Triple (s,p,o)]
  | `Seq [] -> assert false
  | `Seq [pred0] -> sem_pred gv s pred0 o
  | `Seq (pred0::preds1) ->
     let x = gv#new_var in
     sem_pred gv s pred0 x @ sem_pred gv s (`Seq preds1) o
  | `Alt lpred ->
     [`Or (List.map (fun pred1 -> and_ (sem_pred gv s pred1 o)) lpred)]
  | `Inverse pred1 -> sem_pred gv o pred1 s
  | `Star (`Term (Uri p)) -> sem_pred gv s (`Term (Uri (p ^ "*"))) o
  | `Plus (`Term (Uri p)) -> sem_pred gv s (`Term (Uri (p ^ "+"))) o
  | `Opt (`Term (Uri p)) -> sem_pred gv s (`Term (Uri (p ^ "?"))) o
  | _ -> failwith "*/+/? closures are not supported on complex property paths" (* TODO *)
and sem_object gv : Sparql.sparql_object -> term * f list = function
  | `Term o -> o, []
  | `Block lpo ->
     let x = gv#new_var in
     x, sem_descr gv x lpo
and sem_filter gv : Sparql.sparql_expr -> f = function
  | `Term t -> assert false
  | `Appl (op,args) ->
     let lt, lf = sem_args gv args in
     and_ (`Pred (`Appl op, lt) :: lf)
  | `Infix ("&&", [`Infix (">=", [arg1; arg2]); `Infix ("<=", [arg1'; arg3])]) when arg1 = arg1' ->
     let arg1, arg2, arg3 =
       match arg1, arg2, arg3 with (* to handle conversions generated for comparisons with literals *)
       | `Appl (op, [`Appl ("STR", [e1])]), `Term (Literal (_, Datatype _)), `Term (Literal (_, Datatype _)) ->
          e1, arg2, arg3
       | `Appl ("STR", [e1]), `Term (Literal (s2, Bare)), `Term (Literal (s3, Bare)) ->
          e1, `Term (typed_literal s2), `Term (typed_literal s3)
       | _ -> arg1, arg2, arg3 in
     let t1, f1 = sem_expr gv arg1 in
     let t2, f2 = sem_expr gv arg2 in
     let t3, f3 = sem_expr gv arg3 in
     and_ (`Pred (`Appl "BETWEEN", [t1; t2; t3]) :: f1 :: f2 :: f3 :: [])
  | `Infix ("&&",le) -> and_ (List.map (sem_filter gv) le)
  | `Infix ("||",le) -> `Or (List.map (sem_filter gv) le)
  | `Infix (op,args) ->
     let op_sem, is_comp =
       match op with
       | "=" -> `Eq, true
       | ">" -> `Gt, true
       | "<" -> `Lt, true
       | "<=" -> `Leq, true
       | ">=" -> `Geq, true
       | "!=" -> `Neq, true
       | _ -> `Infix op, false in
     let args =
       match is_comp, args with (* to handle conversions generated for comparisons with literals *)
       | true, [`Appl (op, [`Appl ("STR", [e1])]); (`Term (Literal (s, Datatype dt)) as e2)] ->
          [e1; e2]
       | true, [`Appl ("STR", [e1]); (`Term (Literal (s, Bare)) as e2)] ->
          let e2 = `Term (typed_literal s) in
          [e1; e2]
       | _ -> args in
     let lt, lf = sem_args gv args in
     and_ (`Pred (op_sem, lt) :: lf)
  | `Prefix ("!",[e]) -> not_ (sem_filter gv e)
  | `Prefix (op,args) ->
     let lt, lf = sem_args gv args in
     and_ (`Pred (`Prefix op, lt) :: lf)
  | `IN (e1,le2) ->
     let x, f1 = sem_expr gv e1 in
     let ly, lf2 = sem_args gv le2 in
     let f_in =
       `Or (List.map
              (fun y -> sem_filter gv (sparql_equal (`Term x) (`Term y)))
              ly) in
     and_ (f1 :: lf2 @ [f_in])
  | `EXISTS p -> sem_pattern gv p
  | `NOT_EXISTS p -> not_ (sem_pattern gv p)
  | `Star -> assert false
and sem_args gv (le : Sparql.sparql_expr list) : term list * f list =
  List.split (List.map (fun e -> sem_expr gv e) le)
and sem_expr gv : Sparql.sparql_expr -> term * f = function
  | `Term t -> t, `True
  | `Appl (op,args) ->
     let x = gv#new_var in
     let lx1, lf1 = sem_args gv args in
     x, and_ (`Func (`Appl op, lx1, x) :: lf1)
  | `Infix (op,args) ->
     let x = gv#new_var in
     let lx1, lf1 = sem_args gv args in
     x, and_ (`Func (`Infix op, lx1, x) :: lf1)
  | `Prefix (op,args) ->
     let x = gv#new_var in
     let lx1, lf1 = sem_args gv args in
     x, and_ (`Func (`Prefix op, lx1, x) :: lf1)
  | `IN _ -> assert false
  | `EXISTS _ -> assert false
  | `NOT_EXISTS _ -> assert false
  | `Star -> assert false

let sem (q : Sparql.sparql_query) : f =
  sem_query (new genvar) q


(* abstract SQUALL syntax (simplified) *)

type squall_term = string
type squall_mark = string

type squall_conj = [`And | `Or]

type squall_modif = { modif : Semantics.modif; count : bool }
let modif_default = { modif = Semantics.modif_default; count = false }
                 
type squall_kind = S | NP | NP2 | Det | NG1 | NG2 | Rel | VP | OP | CP | PP
                 
type squall_s =
  [ `NP_VP of squall_np * squall_vp
  | `Whether of squall_s
  | `IsThere of squall_np
  | `PP of squall_pp * squall_s
  | `Conj of squall_conj * squall_s list ]
and squall_np =
  [ `PN of squall_term
  | `Det of squall_det * squall_ng1
  | `Of of squall_np2 * squall_np
  | `What
  | `Void
  | `Conj of squall_conj * squall_np list
  | `Not of squall_np ]
and squall_np2 =
  [ `Det of squall_det * squall_ng2
  | `Void ]
and squall_det =
  [ `A | `The | `Which | `HowMany ]
and squall_ng1 =
  [ `Noun1 of squall_modif * squall_term * squall_rel option ]
and squall_ng2 =
  [ `Noun2 of squall_modif * squall_term ]
and squall_adj =
  [ `Adj1 of squall_term * squall_cp
  | `Adj2 of squall_term * squall_op
  | `Adj3 of squall_term * squall_op * string (* prep *) * squall_op
  | `SuchThat of squall_s ]
and squall_rel =
  [ `Adj of squall_adj
  | `That of squall_vp
  | `ThatVoid of squall_s
  | `OfWhich of squall_np2 * squall_vp
  | `Whose of squall_ng2 * squall_vp
  | `AtWhich of squall_mark * squall_term * squall_s
  | `Conj of squall_conj * squall_rel list ]
and squall_vp =
  [ `Verb1 of squall_term * squall_cp
  | `Verb2 of squall_term * squall_op
  | `IsThere
  | `IsAdj of squall_adj
  | `IsNP of squall_np
  | `IsNP2of of squall_np2 * squall_op (* object marker: of *)
  | `HasNP2 of squall_np2 * squall_rel option
  | `HasNoun2 of squall_term * squall_op
  | `PP of squall_pp * squall_vp
  | `Conj of squall_conj * squall_vp list
  | `Not of squall_vp ]
and squall_op =
  [ `NP of squall_np * squall_cp
  | `PP of squall_pp * squall_op
  | `Conj of squall_conj * squall_op list
  | `Not of squall_op ]
and squall_cp =
  [ `Nil
  | `PP of squall_pp * squall_cp
  | `Conj of squall_conj * squall_cp list
  | `Not of squall_cp ]
and squall_pp =
  [ `AtNoun2 of squall_mark * squall_term * squall_np
  | `AtNP2 of squall_mark * squall_np2 * squall_rel option ]

(* printing SQUALL *)

let sep_conj : squall_conj -> string = function
  | `And -> " and "
  | `Or -> " or "

let print_ordinal : int -> _ = ipp
  [ n -> print_int of n; [ 1 -> "st" | 2 -> "nd" | 3 -> "rd" | _ -> "th" ] of (n mod 10) ]

let rec print : squall_s -> _ = ipp
  [ s -> print_s of s; "?"; EOF ]
and print_s : squall_s -> _ = ipp
  [ `NP_VP (np,vp) -> print_np of np; " "; print_vp ~neg:false of vp
  | `Whether s -> "whether "; print_s of s
  | `IsThere np -> "is there "; print_np of np
  | `PP (pp, s) -> print_pp of pp; " "; print_s of s
  | `Conj (`And, ls) -> LIST1 print_s SEP " and " of ls
  | `Conj (`Or, ls) -> LIST1 print_s SEP " or " of ls
  | _ -> "..." ]
and print_np : squall_np -> _ = ipp
  [ `PN t -> print_term of t
  | `Det (det, ng1) -> print_det of det; print_ng1 of ng1
  | `Of (np2,np) -> print_np2 of np2; " of "; print_np of np
  | `What -> "what"
  | `Void ->
  | `Conj (`And, lnp) -> LIST1 print_np SEP " and " of lnp
  | `Conj (`Or, lnp) -> LIST1 print_np SEP " or " of lnp
  | `Not np -> "not "; print_np of np
  | _ -> "..." ]
and print_np2 : squall_np2 -> _ = ipp
  [ `Det (det,ng2) -> print_det of det; print_ng2 of ng2
  | `Void ->
  | _ -> "..." ]
and print_det : squall_det -> _ = ipp
  [ `A -> "a "
  | `The -> "the "
  | `Which -> "which "
  | `HowMany -> "how many "
  | _ -> "..." ]
and print_ng1 : squall_ng1 -> _ = ipp
  [ `Noun1 (modif, n1, rel_opt) -> print_modif of modif; print_term of n1; print_rel_opt of rel_opt
  | _ -> "..." ]
and print_ng2 : squall_ng2 -> _ = ipp
  [ `Noun2 (modif, n2) -> print_modif of modif; print_term of n2
  | _ -> "..." ]
and print_modif : squall_modif -> _ = ipp
  [ { modif = { order=`DESC; offset=0; limit=(-1) }; count } ->
    "descending "; print_numberof of count
  | { modif = { order=`ASC; offset=0; limit=(-1) }; count } ->
     "ascending "; print_numberof of count
  | { modif = { order=`NONE; offset=0; limit=(-1) }; count } ->
  | { modif = { order; offset=0; limit=1 }; count } ->
     print_order of (order, count)
  | { modif = { order; offset=0; limit }; count } ->
     print_int of limit; " "; print_order of (order, count)
  | { modif = { order; offset; limit=(-1) }; count } ->
     print_ordinal of (offset+1); " to last "; print_order of (order, count)
  | { modif = { order; offset; limit=1 }; count } ->
     print_ordinal of (offset+1); " "; print_order of (order, count)
  | { modif = { order; offset; limit }; count } ->
     print_ordinal of (offset+1); " to "; print_ordinal of (offset+limit); " "; print_order of (order, count)
  | _ -> "..." ]
and print_numberof = ipp
  [ false -> 
  | true -> "number of " ]
and print_order = ipp
  [ `DESC, false -> "highest "
  | `DESC, true -> "most "
  | `ASC, false -> "lowest "
  | `ASC, true -> "least "
  | `NONE, count -> "first "; print_numberof of count ]
and print_rel_opt : squall_rel option -> _ = ipp
  [ None ->
  | Some rel -> " "; print_rel of rel
  | _ -> "..." ]
and print_adj : squall_adj -> _ = ipp
  [ `Adj1 (a1,cp) -> print_term of a1; print_cp of cp
  | `Adj2 (a2,op) -> print_term of a2; print_op ~oprep:"" of op
  | `Adj3 (a3,op1,prep,op2) -> print_term of a3; print_op ~oprep:"" of op1; " "; 'prep; print_op ~oprep:"" of op2
  | `SuchThat s -> "such that "; print_s of s ]
and print_rel : squall_rel -> _ = ipp
  [ `Adj adj -> print_adj of adj
  | `That vp -> "that "; print_vp ~neg:false of vp
  | `ThatVoid s -> "that "; print_s of s
  | `OfWhich (np2,vp) -> print_np2 of np2; " of which "; print_vp ~neg:false of vp
  | `Whose (ng2,vp) -> "whose "; print_ng2 of ng2; " "; print_vp ~neg:false of vp
  | `AtWhich (mark,prep,s) -> print_mark of mark; " which "; print_term of prep; " "; print_s of s
  | `Conj (`And, lrel) -> LIST1 print_rel SEP " and " of lrel
  | `Conj (`Or, lrel) -> LIST1 print_rel SEP " or " of lrel
  | _ -> "..." ]
and print_vp ~(neg: bool) : squall_vp -> _ = ipp
  [ `Verb1 (v1,cp) -> [ false -> | true -> "does not " ] of neg; print_term of v1; print_cp of cp
  | `Verb2 (v2,op) -> [ false -> | true -> "does not " ] of neg; print_term of v2; print_op ~oprep:"" of op
  | `IsThere -> [false -> "is there" | true -> "is not there"] of neg
  | `IsAdj adj -> [false -> "is " | true -> "is not "] of neg; print_adj of adj
  | `IsNP np -> [false -> "is " | true -> "is not "] of neg; print_np of np
  | `IsNP2of (np2,op) -> [false -> "is " | true -> "is not "] of neg; print_np2 of np2; print_op ~oprep:"of " of op
  | `HasNP2 (np2,rel_opt) -> [false -> "has " | true -> "has not "] of neg; print_np2 of np2; print_rel_opt of rel_opt
  | `HasNoun2 (n2,op) -> [false -> "has " | true -> "has not "] of neg; print_term of n2; print_op ~oprep:"" of op
  | `PP (pp,vp) -> [false -> | true -> "not "] of neg; print_pp of pp; " "; print_vp ~neg:false of vp
  | `Conj (`And, lvp) ->
     [ false, lvp -> LIST1 print_vp ~neg SEP " and " of lvp
     | true, lvp -> LIST1 print_vp ~neg SEP " or " of lvp ] of (neg,lvp)
  | `Conj (`Or, lvp) ->
     [ false, lvp -> LIST1 print_vp ~neg SEP " or " of lvp
     | true, lvp -> LIST1 print_vp ~neg SEP " and " of lvp ] of (neg,lvp)
  | `Not vp -> print_vp ~neg:(not neg) of vp
  | _ -> "..." ]
and print_op ~(oprep : string) : squall_op -> _ = ipp
  [ `NP (np,cp) -> " "; 'oprep; print_np of np; print_cp of cp
  | `PP (pp,op) -> " "; print_pp of pp; print_op ~oprep of op
  | `Conj (`And, lop) -> LIST1 print_op ~oprep SEP " and " of lop
  | `Conj (`Or, lop) -> LIST1 print_op ~oprep SEP " or " of lop
  | `Not op -> print_op ~oprep of op
  | _ -> "..." ]
and print_cp : squall_cp -> _ = ipp
  [ `Nil -> 
  | `PP (pp,cp) -> " "; print_pp of pp; print_cp of cp
  | `Conj (`And, lcp) -> LIST1 print_cp SEP " and " of lcp
  | `Conj (`Or, lcp) -> LIST1 print_cp SEP " or " of lcp
  | `Not cp -> print_cp of cp
  | _ -> "..." ]
and print_pp : squall_pp -> _ = ipp
  [ `AtNoun2 (mark,n2,np) -> print_mark of mark; " "; print_term of n2; " "; print_np of np
  | `AtNP2 (mark,np2,rel_opt) -> print_mark of mark; " "; print_np2 of np2; print_rel_opt of rel_opt
  | _ -> "..." ]
and print_term = ipp
  [ word -> 'word ]
and print_mark = ipp
  [ mark -> 'mark ]

(* squall utilities *)

let squall_conj (conj : squall_conj) : 'a list -> 'a = function
  | [] -> assert false
  | [x] -> x
  | lx -> `Conj (conj, lx)
          
let rec suffix_kinds_s = function
  (* returns the kinds of all proper suffix phrases, to handle ambiguity in Conj *)
  | `NP_VP (np,vp) -> VP :: suffix_kinds_vp vp
  | `Whether s -> S :: suffix_kinds_s s
  | `IsThere np -> NP :: suffix_kinds_np np
  | `PP (pp,s) -> S :: suffix_kinds_s s
  | `Conj (conj,ls) -> S :: suffix_kinds_s (list_last ls)
and suffix_kinds_np = function
  | `PN _ -> []
  | `Det (det,ng1) -> NG1 :: suffix_kinds_ng1 ng1
  | `Of (np2,np) -> NP :: suffix_kinds_np np
  | `What -> []
  | `Void -> []
  | `Conj (conj,lnp) -> NP :: suffix_kinds_np (list_last lnp)
  | `Not np -> suffix_kinds_np np (* Not has priority on Conj *)
and suffix_kinds_np2 = function
  | `Det (det,ng2) -> NG2 :: suffix_kinds_ng2 ng2
  | `Void -> []
and suffix_kinds_ng1 = function
  | `Noun1 (modif,n1,rel_opt) -> suffix_kinds_rel_opt rel_opt
and suffix_kinds_ng2 = function
  | `Noun2 (modif,n2) -> []
and suffix_kinds_rel_opt = function
  | None -> []
  | Some rel -> Rel :: suffix_kinds_rel rel
and suffix_kinds_adj = function
  | `Adj1 (adj1,cp) -> CP :: suffix_kinds_cp cp
  | `Adj2 (adj2,op) -> OP :: suffix_kinds_op op
  | `Adj3 (adj3,op1,prop,op2) -> OP :: suffix_kinds_op op2
  | `SuchThat s -> S :: suffix_kinds_s s
and suffix_kinds_rel = function
  | `Adj adj -> suffix_kinds_adj adj
  | `That vp -> VP :: suffix_kinds_vp vp
  | `ThatVoid s -> S :: suffix_kinds_s s
  | `OfWhich (np2,vp) -> VP :: suffix_kinds_vp vp
  | `Whose (ng2,vp) -> VP :: suffix_kinds_vp vp
  | `AtWhich (mark,prep,s) -> S :: suffix_kinds_s s
  | `Conj (conj,lrel) -> Rel :: suffix_kinds_rel (list_last lrel)
and suffix_kinds_vp = function
  | `Verb1 (v1,cp) -> CP :: suffix_kinds_cp cp
  | `Verb2 (v2,op) -> OP :: suffix_kinds_op op
  | `IsThere -> []
  | `IsAdj adj -> suffix_kinds_adj adj
  | `IsNP np -> NP :: suffix_kinds_np np
  | `IsNP2of (np2,op) -> OP :: suffix_kinds_op op
  | `HasNP2 (np2,None) -> suffix_kinds_np2 np2
  | `HasNP2 (np2,Some rel) -> suffix_kinds_rel rel
  | `HasNoun2 (n2,op) -> OP :: suffix_kinds_op op
  | `PP (pp,vp) -> VP :: suffix_kinds_vp vp
  | `Conj (conj,lvp) -> VP :: suffix_kinds_vp (list_last lvp)
  | `Not vp -> suffix_kinds_vp vp
and suffix_kinds_op = function
  | `NP (np,`Nil) -> CP :: suffix_kinds_np np
  | `NP (np,cp) -> CP :: suffix_kinds_cp cp
  | `PP (pp,op) -> OP :: suffix_kinds_op op
  | `Conj (conj,lop) -> OP :: suffix_kinds_op (list_last lop)
  | `Not op -> suffix_kinds_op op
and suffix_kinds_cp = function
  | `Nil -> []
  | `PP (pp,cp) -> CP :: suffix_kinds_cp cp
  | `Conj (conj,lcp) -> CP :: suffix_kinds_cp (list_last lcp)
  | `Not cp -> suffix_kinds_cp cp

             
(* from logical formulas to SQUALL *)

let has_prefix (pre : string) : string -> bool =
  let k = String.length pre in
  fun uri ->
  String.length uri > k && String.sub uri 0 k = pre
let has_prefix_p = has_prefix "p:"
let has_prefix_ps = has_prefix "ps:"
let has_prefix_pq =
  let p1 = has_prefix "pq:" in
  let p2 = has_prefix "n1:" in (* TODO: replace n1: by pq: in Mintaka-CNL *)
  fun s -> p1 s || p2 s
  
let replace_prefix (pre1 : string) (pre2 : string) : string -> string =
  let k1 = String.length pre1 in
  fun uri ->
  let n = String.length uri in
  if n > k1
  then pre2 ^ String.sub uri k1 (n-k1)
  else uri
let erase_p = replace_prefix "p:" ":"
let erase_ps = replace_prefix "ps:" ":"
let erase_pq = replace_prefix "pq:" ":"

               
let lexicon_uri (uri : uri) =
  if wikidata then
    match Syntax.wikidata_item uri with
    | Some item -> "<" ^ item ^ ">"
    | None ->
       match Syntax.wikidata_prop uri with
       | Some prop -> "<" ^ prop ^ ">"
       | None -> uri
  else uri

let lexicon_term : term -> string = function
  | Var v -> "?" ^ v
  | Ref _ -> failwith "the verbalization of references is not yet supported" (* TODO *)
  | Uri uri -> lexicon_uri uri
  | Literal (s, ltyp) ->
     let quoted_s = "\"" ^ s ^ "\"" in
     (match ltyp with
      | Bare -> quoted_s 
      | Datatype ("xsd:date" | "xsd:time" | "xsd:dateTime" | "xsd:int" | "xsd:integer" | "xsd:decimal" | "xsd:double" | "xsd:boolean") -> s
      | Datatype dt -> quoted_s ^ "^^" ^ dt
      | Lang lang -> quoted_s ^ "@" ^ lang)
  | GraphLiteral _ -> failwith "the verbalization of graph literals is not supported yet" (* TODO *)
  | Cons _ -> failwith "the verbalization of lists is not supported yet" (* TODO *)

let lexicon_func1 : string (* SPARQL name *) -> string (* noun2 *) = function
  | "STR" -> "string"
  | "LANG" -> "language"
  | "DATATYPE" -> "datatype"
  | "STRLEN" -> "length"
  | "UCASE" -> "uppercase"
  | "LCASE" -> "lowercase"
  | "YEAR" -> "year"
  | "MONTH" -> "month"
  | "DAY" -> "day"
  | "HOURS" -> "hours"
  | "MINUTES" -> "minutes"
  | "SECONDS" -> "seconds"
  | "TIMEZONE" -> "timezone"
  | "TZ" -> "tz"
  | name -> name

let lexicon_pred2 : string (* SPARQL name *) -> string (* adj2 *) = function
  | "STRSTARTS" -> "start with"
  | "STRENDS" -> "end with"
  | "CONTAINS" -> "contain"
  | "REGEX" -> "match"
  | name -> name

let disambiguate_gen kind suffix_kinds (lx : 'a list) : 'a list =
  let lx1, lx2 =
    List.partition
      (fun x -> not (List.mem kind (suffix_kinds x)))
      lx in
  match lx2 with
  | [] -> lx1
  | [_] -> lx1 @ lx2
  | _ -> failwith "the verbalization cannot be made non-ambiguous without brackets"
let disambiguate_vp = disambiguate_gen VP suffix_kinds_vp
let disambiguate_rel = disambiguate_gen Rel suffix_kinds_rel
let disambiguate_op = disambiguate_gen OP suffix_kinds_op
let disambiguate_np = disambiguate_gen NP suffix_kinds_np

let rec group_rel (conj : squall_conj) lrel : squall_rel list =
  match lrel with
  | [] -> []
  | rel::lrel1 ->
     match rel with
     | `Adj (`Adj2 (adj2_pat, _)) ->
        let lop, lrel2 =
          partition_map
            (function
             | `Adj (`Adj2 (adj2, op)) when adj2 = adj2_pat -> Some op
             | _ -> None)
            lrel in
        `Adj (`Adj2 (adj2_pat, squall_conj conj (disambiguate_op (group_op conj lop)))) :: group_rel conj lrel2
     | `That _ ->
        let lvp, lrel2 =
          partition_map
            (function
             | `That rel -> Some rel
             | _ -> None)
            lrel in
        `That (squall_conj conj (disambiguate_vp (group_vp conj lvp))) :: group_rel conj lrel2
     | _ ->
        rel :: group_rel conj lrel1
and group_vp (conj : squall_conj) lvp : squall_vp list =
  match lvp with
  | [] -> []
  | vp::lvp1 ->
     match vp with
     | `Verb2 (v2_pat, _) ->
        let lop, lvp2 =
          partition_map
            (function
             | `Verb2 (v2, op) when v2 = v2_pat -> Some op
             | _ -> None)
            lvp in
        `Verb2 (v2_pat, squall_conj conj (disambiguate_op (group_op conj lop))) :: group_vp conj lvp2
     | `IsAdj (`Adj2 (adj2_pat, _)) ->
        let lop, lvp2 =
          partition_map
            (function
             | `IsAdj (`Adj2 (adj2, op)) when adj2 = adj2_pat -> Some op
             | _ -> None)
            lvp in
        `IsAdj (`Adj2 (adj2_pat, squall_conj conj (disambiguate_op (group_op conj lop)))) :: group_vp conj lvp2
     | `IsNP _ ->
        let lnp, lvp2 =
          partition_map
            (function
             | `IsNP np -> Some np
             | _ -> None)
            lvp in
        `IsNP (squall_conj conj (disambiguate_np lnp)) :: group_vp conj lvp2
     | `IsNP2of (np2_pat,_) ->
        let lop, lvp2 =
          partition_map
            (function
             | `IsNP2of (np2,op) when np2 = np2_pat -> Some op
             | _ -> None)
            lvp in
        `IsNP2of (np2_pat, squall_conj conj (disambiguate_op (group_op conj lop))) :: group_vp conj lvp2
     | `HasNoun2 (n2_pat,_) ->
        let lop, lvp2 =
          partition_map
            (function
             | `HasNoun2 (n2,op) when n2 = n2_pat -> Some op
             | _ -> None)
            lvp in
        `HasNoun2 (n2_pat, squall_conj conj (disambiguate_op (group_op conj lop))) :: group_vp conj lvp2
     | _ ->
        vp :: group_vp conj lvp1
and group_op (conj : squall_conj) (lop : squall_op list) : squall_op list =
  match lop with
  | [] -> []
  | op::lop1 ->
     match op with
     | `NP (_, `Nil) ->
        let lnp, lop2 =
          partition_map
            (function
             | `NP (np, `Nil) -> Some np
             | _ -> None)
            lop in
        `NP (squall_conj conj (disambiguate_np lnp), `Nil) :: group_op conj lop2
     | _ ->
        op :: group_op conj lop1


let conj_rel_opt conj rels : squall_rel option =
  match rels with
  | [] -> None
  | [rel] -> Some rel
  | _ -> Some (squall_conj conj (disambiguate_rel (group_rel conj rels)))
                       
let conj_vp conj vps : squall_vp =
  match vps with
  | [] -> `IsThere
  | [vp] -> vp
  | _ -> squall_conj conj (disambiguate_vp (group_vp conj vps))
                     
let conj_op conj ops : squall_op =
  assert (ops <> []);
  squall_conj conj (disambiguate_op (group_op conj ops))

let np_of_rel : squall_rel -> squall_np = function
  | `That (`IsNP np) -> np
  | `That (`IsNP2of (np2, `NP (np, `Nil))) -> `Of (np2, np)
  | rel -> `Det (`A, `Noun1 (modif_default, "thing", Some rel))

let rec vp_of_rel_opt : squall_rel option -> squall_vp = function
  | None -> `IsThere
  | Some rel -> vp_of_rel rel
and vp_of_rel : squall_rel -> squall_vp = function
  | `Adj adj -> `IsAdj adj
  | `That vp -> vp
  | `OfWhich (np2,vp) -> `HasNP2 (np2, rel_opt_of_vp vp)
  | `Whose (ng2,vp) -> `HasNP2 (`Det (`The, ng2), rel_opt_of_vp vp)
  | `Conj (conj,rels) -> `Conj (conj, List.map vp_of_rel rels)                       
  | rel -> `IsNP (np_of_rel rel)
and rel_opt_of_vp : squall_vp -> squall_rel option = function
  | `IsThere -> None
  | `IsAdj adj -> Some (`Adj adj)
  | `IsNP (`Det (`A, `Noun1 (modif, "thing", rel_opt))) when modif = modif_default -> rel_opt
  | vp -> Some (`That vp)

let vp_of_np : squall_np -> squall_vp = function
  | `Det (`A, `Noun1 (_, "thing", None)) -> `IsThere
  | `Det (`A, `Noun1 (_, "thing", Some rel)) -> vp_of_rel rel
  | np -> `IsNP np
         
let rel_not : squall_rel -> squall_rel = function
  | `That vp -> `That (`Not vp)
  | `Whose (ng2,vp) -> `Whose (ng2, `Not vp)
  | rel -> `That (`Not (vp_of_rel rel))
  
let vp_of_ng1 : squall_ng1 -> squall_vp = function
  | `Noun1 (_, "thing", None) -> `IsThere
  | ng1 -> `IsNP (`Det (`A, ng1))
            
let noun1 n1 : squall_ng1 =
  `Noun1 (modif_default, n1, None)

let noun2 n2 : squall_ng2 =
  `Noun2 (modif_default, n2)
  
let pp_at_noun2 n2 np : squall_pp =
  match np with
  | `Det (det, `Noun1 (modif, "thing", rel_opt)) ->
     `AtNP2 ("at", `Det (det, `Noun2 (modif, n2)), rel_opt)
  | _ -> `AtNoun2 ("at", n2, np)
       
let op_np_args (np : squall_np) (args : (uri * squall_np) list) : squall_op =
  List.fold_right
    (fun (q,np) cp -> `PP (pp_at_noun2 q np, cp))
    args (`NP (np, `Nil))
                             
let has_noun2 n2 np args : squall_vp =
  match np, args with
  | `Det (det, `Noun1 (modif, "thing", rel_opt)), [] ->
     `HasNP2 (`Det (det, `Noun2 (modif, n2)), rel_opt)
  | _ ->
     let op = op_np_args np args in
     `HasNoun2 (n2, op)

let whose_n2_vp n2 vp : squall_rel =
  match vp with
  | `IsThere -> `That (`HasNP2 (`Det (`A, `Noun2 (modif_default, n2)), None))
  | _ -> `Whose (`Noun2 (modif_default, n2), vp)
     
let is_np2_of det n2 np args : squall_vp =
  let op = op_np_args np args in
  `IsNP2of (`Det (det, `Noun2 (modif_default, n2)), op)

let at_which prep np_s n2 np_o args : squall_rel =
  let vp = has_noun2 n2 np_o args in
  let s = `NP_VP (np_s, vp) in
  `AtWhich ("at", prep, s)

let np_det (det : squall_det) ?(modif = modif_default) (n1 : squall_term) (rel_opt : squall_rel option) : squall_np =
  match det, n1, rel_opt with
  | `Which, "thing", None -> `What
  | _ -> `Det (det, `Noun1 (modif, n1, rel_opt))
  
let extract_head_n1 (rels : squall_rel list) : squall_term (* n1 *) * squall_rel list =
  let rec aux = function
    | `That (`IsNP (`Det (`A, `Noun1 (_, n1, rel2_opt)))) :: rels1 ->
       let rels1 =
         match rel2_opt with
         | Some (`Conj (`And, rels2)) -> rels2 @ rels1
         | Some rel2 -> rel2 :: rels1
         | None -> rels1 in
       n1, rels1
    | rel0 :: rels1 ->
       let n1, rels1 = aux rels1 in
       n1, rel0::rels1
    | [] -> "thing", []
  in
  aux rels
                       
let np_of_rels (modif_opt : squall_modif option) (rels : squall_rel list) : squall_np =
  let det, modif =
    match modif_opt with
    | Some modif -> `The, modif
    | None -> `A, modif_default in
  match rels with
  | [`That (`IsNP2of (`Det ((`A | `The as det2), `Noun2 (_, n2)), `NP (np, `Nil)))]
    | [`That (`IsNP (`Of (`Det ((`A | `The as det2), `Noun2 (_, n2)), np)))] ->
     let det = if det = `A && det2 = `The then `The else det in
     `Of (`Det (det, `Noun2 (modif, n2)), np)
  | _ ->
     let n1, rels = extract_head_n1 rels in
     let rel_opt = conj_rel_opt `And rels in
     np_det det ~modif n1 rel_opt

let vp_of_rels (rels : squall_rel list) : squall_vp =
  let vps = List.map vp_of_rel rels in
  conj_vp `And vps
   
let s_of_rels (det : squall_det) (modif_opt : squall_modif option) (rels : squall_rel list) : squall_s =
  match modif_opt with
  | Some _ ->
     let np = np_det det "thing" None in
     let np_rels = np_of_rels modif_opt rels in
     `NP_VP (np, `IsNP np_rels)
  | None ->
     ( match rels with
       | [ `OfWhich (np2,vp) ] -> `NP_VP (`Of (np2, np_det det "thing" None), vp)
       | [ `Whose (ng2,vp) ] -> `NP_VP (`Of (`Det (`The, ng2), np_det det "thing" None), vp)
       | [ `AtWhich (mark,prep,s) ] -> `PP (`AtNP2 (mark, `Det (det, `Noun2 (modif_default, prep)), None), s)
       | _ ->
          let n1, rels = extract_head_n1 rels in
          let np = np_det det n1 None in
          let vp = vp_of_rels rels in
          `NP_VP (np,vp) )

let s_of_term_rels (t : term) (rels : squall_rel list) : squall_s =
  match t with
  | Var _ ->
     let np = np_of_rels None rels in
     `IsThere np
  | _ ->
     let vp = vp_of_rels rels in
     `Whether (`NP_VP (`PN (lexicon_term t), vp))


let remove_f f lf = List.filter ((<>) f) lf

let rec formula_head_entity : f -> term option = function
  | `Triple (s,_,_) -> Some s
  | `Context (op,g,f1) -> formula_head_entity f1
  | `Exists (ly,f1) -> formula_head_entity f1
  | `And lf1 -> List.find_map formula_head_entity lf1
  | _ -> None
                  
let rec squall : f -> squall_s = function
  | `Select ([], f) (* Ask *) ->
     let x =
       match formula_head_entity f with
       | Some x -> x
       | None -> failwith "no head entity could be found in the graph pattern" in
     let rels, seen, lf_remainder = squall_var [x] [f] ~modifs:[] x in
     assert (lf_remainder = []);
     s_of_term_rels x rels
  | `Select ((Var _ as x)::_, f) ->
     let modifs, f1 =
       match f with
       | `Modif (`Mod modif, _lz, y,
                 `Aggreg (`AggregOp1 (`Count, (Var xgy as gy)), gn, [gz], f1))
            when gz = x && gn = y -> [(gy, {modif; count=true})], f1
       | `Modif (`Mod modif, _lz, y, f1) -> [(y, {modif; count=false})], f1
       | _ -> [], f in
     let det, x, f1 =
       match f1 with
       | `Aggreg (`AggregOp1 (`Count, (Var _ as x1)), (Var _ as xn), [], f1) when xn = x ->
          `HowMany, x1, f1
       | _ -> `Which, x, f1 in
     let modif_opt = List.assoc_opt x modifs in
     let rels, seen, lf_remainder = squall_var [x] [f1] ~modifs x in
     if lf_remainder <> [] then
       failwith "some parts of the query could not be verbalized, probably because they are disconnected from the projected variables";
     s_of_rels det modif_opt rels
  | _ -> failwith "the verbalization is only supported for ASK/SELECT queries so far" (* TODO *)
and squall_f (seen : term list) (lf : f list) ~(modifs : (term * squall_modif) list) (x : term) (f : f) : squall_rel list * term list * f list =
  match f with
  | `Select _ -> failwith "the verbalization of sub-queries is not supported yet" (* TODO *)
  | `Triple (s, Uri p, o) ->
     if s = x && (p = "rdf:type" || wikidata && p = "wdt:P31") then
       let lf = remove_f f lf in
       [`That (`IsNP (`Det (`A, noun1 (lexicon_term o))))], (* assuming [o] a URI *)
       seen, lf
     else if s = x then
       let lf = remove_f f lf in
       if wikidata && has_prefix_p p then
         let prop = erase_p p in
         let prop, args, seen, lf = squall_statement seen lf ~modifs ~source:(`S) prop o in
         let np =
           match List.assoc_opt "O" args with
           | Some np -> np
           | _ -> failwith "the object of a reified statement is missing" in
         let args = List.remove_assoc "O" args in
         [`That (has_noun2 (lexicon_uri prop) np args)],
         seen, lf
       else
         let np, seen, lf = squall_term seen lf ~modifs o in
         [`That (has_noun2 (lexicon_uri p) np [])],
         seen, lf
     else if o = x then
       let lf = remove_f f lf in
       if wikidata && has_prefix_ps p then
         let prop = erase_ps p in
         let prop, args, seen, lf = squall_statement seen lf ~modifs ~source:(`O) prop s in
         let np =
           match List.assoc_opt "S" args with
           | Some np -> np
           | _ -> failwith "the subject of a reified statement is missing" in
         let args = List.remove_assoc "S" args in
         [`That (is_np2_of `A (lexicon_uri prop) np args)],
         seen, lf
       else if wikidata && has_prefix_pq p then
         let qual = erase_pq p in
         let prop, args, seen, lf = squall_statement seen lf ~modifs ~source:(`Q) "" s in
         let np_s =
           match List.assoc_opt "S" args with
           | Some np -> np
           | _ -> failwith "the subject of a reified statement is missing" in
         let np_o =
           match List.assoc_opt "O" args with
           | Some np -> np
           | _ -> failwith "the object of a reified statement is missing" in
         let args = List.remove_assoc "S" args in
         let args = List.remove_assoc "O" args in
         [at_which (lexicon_uri qual) np_s (lexicon_uri prop) np_o args],
         seen, lf
       else
         let np, seen, lf = squall_term seen lf ~modifs s in
         [`That (is_np2_of `A (lexicon_uri p) np [])],
         seen, lf
     else
       [], seen, lf
  | `Triple _ -> failwith "the verbalization of triple patterns with a variable or complex predicate is not supported yet" (* TODO *)
  | `Unify _ -> assert false
  | `Label _ -> assert false
  | `Pred (op, [t1]) ->
     if t1 = x then
       let lf = remove_f f lf in
       (match op with
        | `Appl pred -> (* assuming 'pred' is a verb *)
           [`That (`Verb1 (pred, `Nil))],
           seen, lf
        | _ -> failwith "the verbalization of infix and prefix unary predicates are not supported" (* TODO *) )
     else
       [], seen, lf
  | `Pred (op, [t1;t2]) ->
     let on1, on2 = t1 = x, t2 = x in
     if on1 || on2 then
       let lf = remove_f f lf in
       (match op with
        | `Appl pred -> (* assuming 'pred' is a verb *)
           let v2_pred = lexicon_pred2 pred in
           if on1 then
             let np, seen, lf = squall_term seen lf ~modifs t2 in
             [`That (`Verb2 (v2_pred, `NP (np, `Nil)))],
             seen, lf
           else
             let np, seen, lf = squall_term seen lf ~modifs t1 in
             [`ThatVoid (`NP_VP (np, `Verb2 (v2_pred, `NP (`Void, `Nil))))],
             seen, lf
        | `Eq ->
           let np, seen, lf = squall_term seen lf ~modifs (if on1 then t2 else t1) in
           [`That (`IsNP np)],
           seen, lf
        | `Neq ->
           let np, seen, lf = squall_term seen lf ~modifs (if on1 then t2 else t1) in
           [`That (`Not (`IsNP np))],
           seen, lf
        | (* `Eq | `Neq | *) `Lt | `Leq | `Gt | `Geq as op ->
           let adj_op, t =
             match op with
             (* | `Eq -> "equal to", if on1 then t2 else t1
             | `Neq -> "different from", if on1 then t2 else t1 *)
             | `Lt -> if on1 then "lesser than", t2 else "greater than", t1
             | `Leq -> if on1 then "lesser or equal to", t2 else "greater or equal to", t1
             | `Gt -> if on1 then "greater than", t2 else "lesser than", t1
             | `Geq -> if on1 then "greater or equal to", t2 else "lesser or equal to", t1 in
           let np, seen, lf = squall_term seen lf ~modifs t in
           [`Adj (`Adj2 (adj_op, `NP (np, `Nil)))],
           seen, lf
        | _ -> failwith "the verbalization of infix and prefix binary predicates are not supported" (* TODO *) )
     else
       [], seen, lf
  | `Pred (`Appl "REGEX", [t1;t2;_]) -> (* ignoring the 3rd arg *)
     if t1 = x then
       let lf = remove_f f lf in
       let np, seen, lf = squall_term seen lf ~modifs t2 in
       [`That (`Verb2 ("matches", `NP (np, `Nil)))],
       seen, lf
     else if t2 = x then
       let lf = remove_f f lf in
       let np, seen, lf = squall_term seen lf ~modifs t1 in
       [`ThatVoid (`NP_VP (np, `Verb2 ("matches", `NP (`Void, `Nil))))],
       seen, lf
     else
       [], seen, lf
  | `Pred (`Appl "BETWEEN", [t1;t2;t3]) ->
     if t1 = x then
       let lf = remove_f f lf in
       let np2, seen, lf = squall_term seen lf ~modifs t2 in
       let np3, seen, lf = squall_term seen lf ~modifs t3 in
       [`Adj (`Adj3 ("between", `NP (np2, `Nil), "and", `NP (np3, `Nil)))],
       seen, lf
     else
       [], seen, lf
  | `Pred _ -> failwith "the verbalization of n-ary predicates (n>2) are not supported yet" (* TODO *)
  | `Func (`Appl func, [arg], res) -> (* only applicative unary function *)
     if res = x then
       let lf = remove_f f lf in
       let np, seen, lf = squall_term seen lf ~modifs arg in
       [`That (is_np2_of `The (lexicon_func1 func) np [])],
       seen, lf
     else if arg = x then
       let lf = remove_f f lf in
       let np, seen, lf = squall_term seen lf ~modifs res in
       [whose_n2_vp (lexicon_func1 func) (vp_of_np np)],
       seen, lf
     else
       [], seen, lf
  | `Func _ -> failwith "the verbalization of n-ary functions (n>1) are not supported yet" (* TODO *)
  | `Matches _ -> assert false
  | `Proc _ -> assert false
  | `Truth _ -> assert false
  | `Context _ -> assert false
  | `Aggreg _ -> failwith "the verbalization of aggregates is not supported yet" (* TODO *)
  | `Modif _ -> assert false
  | `Exists _ -> assert false
  | `Forall _ -> assert false
  | `Cond _ -> assert false
  | `True ->
     [], seen, lf
  | `And lf1 ->
     let lf = remove_f f lf in
     squall_var seen (lf1 @ lf) ~modifs x
  | `Or lf1 ->
     let lres =
       List.map
         (fun f1 -> squall_var seen [f1] ~modifs:[] x)
         lf1 in
     if List.for_all
          (fun (rels, seen, lf) -> rels <> [])
          lres
     then
       let lf = remove_f f lf in
       let seen0 = seen in
       let seen =
         List.fold_left
           (fun seen (_, seen1, _) ->
             let pre1 = list_prefix_of_suffix seen1 seen0 in
             pre1 @ seen)
           seen0 lres in
       let rels1 =
         List.map
           (fun (rels, _, lf) ->
             assert (lf = []);
             match conj_rel_opt `And rels with
             | None -> assert false
             | Some rel -> rel)
           lres in
       let rel =
         match conj_rel_opt `Or rels1 with
         | None -> assert false (* lf1 = []? *)
         | Some rel -> rel in
       [rel], seen, lf
     else [], seen, lf
  | `Not f1 ->
     let rels1, seen1, lf1 = squall_var seen [f1] ~modifs:[] x in
     if rels1 <> []
     then
       let _ = assert (lf1 = []) in
       let seen = seen1 in
       let lf = remove_f f lf in
       let rel =
         match conj_rel_opt `And rels1 with
         | None -> assert false
         | Some rel -> rel_not rel in
       [rel], seen, lf
     else [], seen, lf
  | `Option _ -> failwith "the verbalization of OPTIONAL is not supported yet"
and squall_var seen lf ~modifs x =
  let rev_rels, seen, lf =
    List.fold_left (* TODO: custom iteration to avoid visiting consumed formulas *)
      (fun (rev_rels,seen,lf) f ->
        let rels1, seen, lf = squall_f seen lf ~modifs x f in
        List.rev_append rels1 rev_rels, seen, lf)
      ([], seen, lf) lf in
  List.rev rev_rels, seen, lf
and squall_term seen lf ~modifs t =
  match t with
  | (Var _ as x) ->
     if List.mem x seen then failwith "the verbalization of cyclic graph patterns is not supported yet"; (* TODO *)
     let modif_opt = List.assoc_opt x modifs in
     let rels, seen, lf = squall_var (x::seen) lf ~modifs x in
     np_of_rels modif_opt rels, seen, lf
  | _ ->
     `PN (lexicon_term t), seen, lf
and squall_statement seen lf ~modifs ~(source : [`S|`O|`Q]) prop0 x = (* for Wikidata reification *)
  let prop, rev_args, seen, lf =
    List.fold_left (* TODO: custom iteration to avoid visiting consumed formulas *)
      (fun (prop, args, seen, lf) f ->
        match source, f with
        | (`S|`Q), `Triple (s, Uri p, o) when s = x && has_prefix_ps p ->
           let prop = erase_ps p in
           if source = `S && prop <> prop0 then failwith ("ill-formed reified statement: expected ps:" ^ prop0);
           let lf = remove_f f lf in
           let np, seen, lf = squall_term seen lf ~modifs o in (* object *)
           if List.mem_assoc "O" args then failwith "ill-formed reified statement: duplicate object";
           prop, ("O",np)::args, seen, lf
        | (`O|`Q), `Triple (s, Uri p, o) when o = x && has_prefix_p p ->
           let prop = erase_p p in
           if source = `O && prop <> prop0 then failwith ("ill-formed reified statement: expected p:" ^ prop0);
           let lf = remove_f f lf in
           let np, seen, lf = squall_term seen lf ~modifs s in (* subject *)
           if List.mem_assoc "S" args then failwith "ill-formed reified statement: duplicate subject";
           prop, ("S",np)::args, seen, lf
        | _, `Triple (s, Uri p, o) when s = x && not (has_prefix_p p || has_prefix_ps p) ->
           let lf = remove_f f lf in
           let np, seen, lf = squall_term seen lf ~modifs o in (* qualifier *)
           prop, (lexicon_uri p, np)::args, seen, lf
        | _ -> prop, args, seen, lf)
      (prop0, [], seen, lf) lf in
  prop, List.rev rev_args, seen, lf              
