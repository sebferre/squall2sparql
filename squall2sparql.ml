(*
    This file is part of 'squall2sparql' <http://www.irisa.fr/LIS/softwares/squall/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(*
   Command line version of SQUALL2SPARQL.
   Usage: read a SQUALL sentence as arg or stdin, and write a SPARQL sentence on stdout.
*)

let _ =
  try
    let sentence_opt = ref None in
    Arg.parse [
        "-wikidata", Arg.Unit (fun () -> ()), "target Wikidata schema (P31 for rdf:type, and reified statements)"; (* already set in semantics.ml *)
      ]
      (fun s -> sentence_opt := Some s)
      "./squall2sparql [-wikidata] [sentence]";
    let context = new Sparql.context (*Dbpedia.context*) in
    let _, sem =
      let cursor =
        match !sentence_opt with
        | Some s -> Matcher.cursor_of_string s
        | None -> Matcher.cursor_of_channel stdin in
      Dcg.once Syntax.parse context cursor in
    let sem = Semantics.validate sem in
    try
      let sparql = Sparql.of_semantics sem in
      let () = (* printing the resulting SPARQL *)
        let cursor = Printer.cursor_of_formatter Format.std_formatter in
        Ipp.once Sparql.print sparql cursor context;
        Format.print_newline () in
(*      let str_sparql = Format.flush_str_formatter () in
      print_endline str_sparql in
    let () = (* testing round parsing of generated SPARQL *)
      match sparql with
      | [`ASK _ | `SELECT _] ->
         let cursor_parse = Matcher.cursor_of_string str_sparql in
         let sparql2 : Sparql.sparql_query = snd (Dcg.once Sparql.parse () cursor_parse) in
         let cursor_print = Printer.cursor_of_formatter Format.str_formatter in
         Ipp.once Sparql.print [sparql2] cursor_print context;
         let str_sparql2 = Format.flush_str_formatter () in
         if str_sparql2 <> str_sparql then (
           print_endline "WARNING: the sparql query was parsed and printed to something different:";
           print_endline str_sparql2)
      | _ -> () (* other query forms are not yet supported by Sparql.parse *) in
 *)
      exit 0
    with exn ->
      prerr_string "SEM: ";
      Semantics.output_sem Format.err_formatter context sem;
      prerr_newline ();
      raise exn
  with exn ->
    prerr_string (Printexc.to_string exn);
    prerr_newline ();
    exit 1
