(*
    This file is part of 'squall2sparql' <http://www.irisa.fr/LIS/softwares/squall/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

type errors = { failures : string list;
		gets : string list;
		looks : string list;
		eof : bool;
		expects : string list }

let rec list_rev_undup = function
  | [] -> []
  | x::xs ->
    if List.mem x xs
    then list_rev_undup xs
    else x::list_rev_undup xs

let print_sem context sem =
  print_string "SEM: ";
  Semantics.output_sem Format.std_formatter context sem;
  print_newline ()

(*let skip = Str.regexp ""*)

let step () =
  let line = read_line () in
  if List.mem line ["exit"; "quit"]
  then false
  else
    try
      let context = new Sparql.context (*Dbpedia.context*) in
      let sols =
	if Array.length Sys.argv > 1 && Sys.argv.(1) = "all"
	then Dcg.all Syntax.parse context (Matcher.cursor_of_string (*skip*) line)
	else [Dcg.once Syntax.parse context (Matcher.cursor_of_string (*skip*) line)] in
      let sols = List.map snd sols in
      let sols = list_rev_undup sols in
      List.iter
	(fun sem ->
	  (*print_sem context sem;*)
	  let sem = Semantics.validate sem in
	  print_sem context sem;
          let ast_sparql = Sparql.of_semantics sem in
	  let sparql =
	    let cursor = Printer.cursor_of_formatter (Format.str_formatter) in
	    Ipp.once Sparql.print ast_sparql cursor context;
	    Format.flush_str_formatter () in
	  print_string "SPARQL > ";
	  print_endline sparql;
          let sem2 =
            match ast_sparql with
            | [`ASK _ | `SELECT _] ->
               let ast_sparql2 = snd (Dcg.once Sparql.parse () (Matcher.cursor_of_string sparql)) in
               let sem2 = Backtranslation.sem ast_sparql2 in
               print_sem context sem2;
               let squall2 =
                 try
                   let ast_squall2 = Backtranslation.squall sem2 in
                   let cursor = Printer.cursor_of_formatter (Format.str_formatter) in
                   Ipp.once Backtranslation.print ast_squall2 cursor context;
                   Format.flush_str_formatter ()
                 with exn -> Printexc.to_string exn in
               print_string "SQUALL > ";
               print_endline squall2;
               ()
            | _ -> () in
          ())
	sols;
      true
    with
      | Dcg.SyntaxError (line, col, exns) ->
	let errors =
	  List.fold_left
	    (fun errors -> function
	      | Failure s -> {errors with failures = s::errors.failures}
	      | Matcher.Get s -> {errors with gets = s::errors.gets}
	      | Matcher.Look s -> {errors with looks = s::errors.looks}
	      | Matcher.Eof -> {errors with eof = true}
	      | Syntax.Expect s -> {errors with expects = s::errors.expects}
	      | _ -> errors)
	    {failures=[]; gets=[]; looks=[]; eof=false; expects=[]}
	    exns in
	let print_errors kind ls =
	  print_string "- "; print_string kind; print_string ": ";
	  List.iter (fun s -> print_string s; print_string ", ") ls;
	  print_newline () in
	Printf.printf "On line %d at position %d\n" line col;
	print_errors "failures" errors.failures;
	print_errors "get-expected" errors.gets;
	print_errors "look-expected" errors.looks;
	print_errors "expected" errors.expects;
	true
      | exn ->
	print_endline (Printexc.to_string exn);
	true

let prompt () = print_string "squall < "

let _ =
  prompt ();
  while step () do
    prompt ()
  done

