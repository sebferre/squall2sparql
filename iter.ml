(*
   Reads lines from input, one per line.
   Prints their result by applying some command, one per line.
*)

let _ =
  try
    let cmd = Sys.argv.(1) in
    let pos = ref 0 in
    while true do
      incr pos;
      let line = read_line () in
      print_endline line;
      if line <> "" && line.[0] <> '#' then (
	let code = Sys.command ("./" ^ cmd ^ " \"" ^ String.escaped line ^ "\"") in
	if code <> 0
	then (
	  prerr_newline ();
          prerr_endline ("Error in line " ^ string_of_int !pos);
	  prerr_endline line;
	  exit 1)
	else (
	  prerr_string ".";
	  flush stderr)
      )
    done
  with
    | End_of_file -> prerr_newline ()
